PRO IDLgrTree::HandleEvent, ev

  ;右键菜单
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  selection = WIDGET_INFO(ev.ID, /tree_select)
  
  (*pState).OTREE.GETPROPERTY, FIDNOW = Fid
  
  IF TAG_NAMES(ev, /structure_name) EQ 'WIDGET_CONTEXT' THEN BEGIN
  
    ;显示右键
    ;空白处右键
    IF selection EQ -1 AND fid GT 0 THEN BEGIN
    
      Blank_contextBase = WIDGET_INFO(ev.TOP, find_by_uname = 'Blank_contextMenu')
      WIDGET_DISPLAYCONTEXTMENU, ev.ID, ev.X, ev.Y, Blank_contextBase
      
    ENDIF ELSE IF selection NE -1 THEN BEGIN
    
      ;拆分uname，判断File or Band
      uname = WIDGET_INFO(selection, /uname)
      
      ;如果uname为空，即投影信息，显示与空白处右键相同的菜单
      IF uname EQ '' THEN BEGIN
        Blank_contextBase = WIDGET_INFO(ev.TOP, find_by_uname = 'Blank_contextMenu')
        WIDGET_DISPLAYCONTEXTMENU, ev.ID, ev.X, ev.Y, Blank_contextBase
        RETURN
      ENDIF
      
      file_or_band = STRMID(uname, 0, 11)
      
      IF  file_or_band EQ 'file_branch' THEN BEGIN
      
        ;文件处显示右键
        File_contextBase = WIDGET_INFO(ev.TOP, find_by_uname = 'File_contextMenu')
        WIDGET_DISPLAYCONTEXTMENU, ev.ID, ev.X, ev.Y, File_contextBase
        
      ENDIF ELSE BEGIN
      
        ;波段处显示右键
        Band_contextBase = WIDGET_INFO(ev.TOP, find_by_uname = 'Band_contextMenu')
        WIDGET_DISPLAYCONTEXTMENU, ev.ID, ev.X, ev.Y, Band_contextBase
        
      ENDELSE
    ENDIF
  ENDIF
END




PRO IDLgrTree::LoadBand, ev
  ;
  ;右键Load波段事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  ;获取所选文件名 - file
  (*pState).OTREE.GETPROPERTY, FIDNOW = Fid
  file = ((*pState).FIDHASH EQ fid)[0]
  
  ;获取显示数据 - data
  (*pState).OTREE.GETPROPERTY, BANDGRAY_IDX = channels
  data = REVERSE(READ_TIFF(file, channels = channels), 2)
  R = QUERY_TIFF(file, info)
  
  ;把数据set到oImage的data属性
  (*pState).OIMAGE.SETPROPERTY, data = TEMPORARY(LINEAR2(data))
  
  IF fid NE (*pState).VIEWFID THEN BEGIN
  
    ;调整viewPlane_Rect，居中显示原始大小图像
    drawSizeALL = WIDGET_INFO((*pState).WDRAW, /geom)
    drawSize = [drawSizeALL.XSIZE, drawSizeALL.YSIZE]
    vp = INTARR(4)
    vp[0] = -(drawSize[0]-info.DIMENSIONS[0])/2
    vp[1] = -(drawSize[1]-info.DIMENSIONS[1])/2
    vp[2:3] = drawSize[0:1]
    
    (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
    
  ENDIF
  
  ;设置Gray单选框选中状态
  (*pState).OMODE.GETPROPERTY, GRAY_BUTTON = GRAY_BUTTON
  WIDGET_CONTROL, GRAY_BUTTON, /set_button
  (*pState).OMODE.CHANGE2GRAY, ev
  
  ;将FID和Band记录下来
  (*pState).VIEWFID = fid
  (*pState).VIEWBAND = channels
  
  ;刷新显示
  IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.LOADIMAGE
  
  IDLVIEWER_REFRESHDRAW, pState
  
END




PRO IDLgrTree::RemoveFile, ev

  ;右键移除选定文件事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  
  ;获取所选文件，并删除
  (*pState).OTREE.GETPROPERTY, FIDNOW = Fidnow
  file = ((*pState).FIDHASH EQ fidnow)[0]
  ((*pState).FIDHASH).REMOVE, file
  
  ;获取所选文件branch的uname，并利用uname获取id，然后销毁
  uname = WIDGET_INFO(WIDGET_INFO(((*pState).OTREE).ID, /tree_select), /uname)
  file_branch = WIDGET_INFO(((*pState).OTREE).ID, find_by_uname = uname)
  WIDGET_CONTROL, file_branch, /destroy
  
  ;自动选中最上边的文件的第一波段
  N = N_ELEMENTS((*pState).FIDHASH)
  
  IF N GT 0 THEN BEGIN
  
    TopChild = (WIDGET_INFO(((*pState).OTREE).ID, /all_children))[0]
    uname = WIDGET_INFO(TopChild, /uname)
    Fidtop = LONG(STRMID(uname, 12))
    
    file = ((*pState).FIDHASH EQ Fidtop)[0]
    pos = STRPOS(file, '\', /reverse_search)
    filename = STRMID(file, pos+1)
    data = REVERSE(READ_TIFF(file, channels = 0), 2)
    R = QUERY_TIFF(file, info)
    ;
    Leaf = WIDGET_INFO(ev.TOP, find_by_uname = 'band_branch_0 ' + STRTRIM(STRING(Fidtop),2))
    IF N_ELEMENTS(leaf) NE 0 THEN WIDGET_CONTROL, Leaf, /set_tree_select
    
    bandname = 'Band 1:' + filename
    
    ;设置DimsText
    dims = STRTRIM(STRING(info.DIMENSIONS[0]),2) + '×' + STRTRIM(STRING(info.DIMENSIONS[1]),2)
    DimsText = TEMPORARY(dims) + ' (' + TYPENAME(data) + ') ' + '[BSQ]'
    
    ;设置oTree属性
    (*pState).OTREE.SETPROPERTY, BandRGB_idx = [0,-1,-1]
    (*pState).OTREE.SETPROPERTY, FidRGB = [Fidtop,-1,-1]
    (*pState).OTREE.SETPROPERTY, BandRGB_name = [bandname,'','']
    
    (*pState).OTREE.SETPROPERTY, BandGray_idx = 0
    (*pState).OTREE.SETPROPERTY, BandGray_name = bandname
    
    (*pState).OTREE.SETPROPERTY, FIDNOW = Fidtop
    (*pState).OMODE.SETPROPERTY, RGB_IDX = 0
    
  ENDIF ELSE BEGIN
  
    ;如果列表中无文件
    (*pState).OTREE.SETPROPERTY, BandRGB_idx = [-1,-1,-1]
    (*pState).OTREE.SETPROPERTY, FidRGB = [-1,-1,-1]
    (*pState).OTREE.SETPROPERTY, BandRGB_name = ['','','']
    
    (*pState).OTREE.SETPROPERTY, BandGray_idx = -1
    (*pState).OTREE.SETPROPERTY, BandGray_name = ''
    
    (*pState).OTREE.SETPROPERTY, FIDNOW = -1
    (*pState).OMODE.SETPROPERTY, RGB_IDX = 0
    
    (*pState).VIEWIDX = 0
    (*pState).MOUSESTATUS = ''
    
    DimsText = ''
    
    (*pState).VIEWFID_RGB = [-1,-1,-1]
    (*pState).VIEWBAND = -1
    (*pState).VIEWBAND_RGB = [-1,-1,-1]
    
    ;鹰眼图内容更新
    IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.SETPROPERTY, clear = 1
    
    ;设置工具栏按钮风格
    IDLVIEWER_BNTSTYLE, ev, 0
    (*pState).OWIN.SETCURRENTCURSOR, 'original'
    
  ENDELSE
  
  
  ;设置DimsText属性
  WIDGET_CONTROL, ((*pState).OMODE).DIMSTEXT, set_value = DimsText
  
  ;修改已选波段列表
  IF ((*pState).OMODE).INDEX EQ 1 THEN BEGIN
  
    ;RGB模式下
    FOR i =0, 2 DO BEGIN
      WIDGET_CONTROL, (((*pState).OMODE).RGB_BAND)[i], set_value = (((*pState).OTREE).BANDRGB_NAME)[i]
    ENDFOR
    
    ;设置R单选按钮选中状态
    WIDGET_CONTROL, (((*pState).OMODE).RGB_BAND)[3], /set_button
    
  ENDIF ELSE BEGIN
    ;Gray模式下
    WIDGET_CONTROL, ((*pState).OMODE).GRAY_BAND, set_value = ((*pState).OTREE).BANDGRAY_NAME
  ENDELSE
  
  ;判断oImage中的数据是否为fidnow
  IF (*pState).VIEWFID EQ fidnow THEN BEGIN
  
    (*pState).VIEWFID = 0
    ;
    ;如果相同，则删除oImage中data数据，并重新绘制
    OBJ_DESTROY, (*pState).OIMAGE
    (*pState).OIMAGE = OBJ_NEW('IDLgrImage')
    (*pState).OMODEL.ADD, (*pState).OIMAGE
    
    OBJ_DESTROY, (*pState).OPOLYGON
    (*pState).OPOLYGON = OBJ_NEW('IDLgrPolygon')
    (*pState).OMODEL.ADD, (*pState).OPOLYGON
    
    
    ;鹰眼图内容更新
    IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.SETPROPERTY, clear = 1
    IDLVIEWER_REFRESHDRAW, pState
    
  ENDIF
  
END



PRO IDLgrTree::CloseAllFiles, ev

  ;关闭所有文件事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  ;文件列表销毁
  file_ids = WIDGET_INFO(((*pState).OTREE).ID, /all_children)
  IF file_ids[0] EQ 0 THEN RETURN
  FOREACH element, file_ids DO WIDGET_CONTROL, element, /destroy
  
  ;文件ID关闭
  (*pState).FIDHASH = HASH()
  
  ;设置DimsText
  WIDGET_CONTROL, ((*pState).OMODE).DIMSTEXT, set_value = ''
  
  ;设置oTree属性
  (*pState).OTREE.SETPROPERTY, BandRGB_idx = [-1,-1,-1]
  (*pState).OTREE.SETPROPERTY, FidRGB = [-1,-1,-1]
  (*pState).OTREE.SETPROPERTY, BandRGB_name = ['','','']
  
  (*pState).OTREE.SETPROPERTY, BandGray_idx = -1
  (*pState).OTREE.SETPROPERTY, BandGray_name = ''
  
  (*pState).OTREE.SETPROPERTY, FIDNOW = -1
  (*pState).OMODE.SETPROPERTY, RGB_IDX = 0
  
  ;打开Gray模式
  ((*pState).OMODE).CHANGE2GRAY, ev
  
  ;设置按钮图片
  IDLVIEWER_BNTSTYLE, ev, 0
  
  ;鹰眼图内容更新
  IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.SETPROPERTY, clear = 1
  
  ;关闭显示图像
  OBJ_DESTROY, (*pState).OIMAGE
  (*pState).OIMAGE = OBJ_NEW('IDLgrImage')
  (*pState).OMODEL.ADD, (*pState).OIMAGE
  
  OBJ_DESTROY, (*pState).OPOLYGON
  (*pState).OPOLYGON = OBJ_NEW('IDLgrPolygon')
  (*pState).OMODEL.ADD, (*pState).OPOLYGON
  
  (*pState).VIEWIDX = 0
  
  (*pState).VIEWFID = -1
  (*pState).VIEWFID_RGB = [-1,-1,-1]
  (*pState).VIEWBAND = -1
  (*pState).VIEWBAND_RGB = [-1,-1,-1]
  
  IDLVIEWER_REFRESHDRAW, pState
  
END




PRO IDLgrTree::GetFileList, filelist = filelist

  ;获取已经打开的文件列表
  COMPILE_OPT idl2
  WIDGET_CONTROL, (*!ev).TOP, get_uvalue = pState
  
  child = WIDGET_INFO(((*pState).OTREE).ID, /all_children)
  
  IF child[0] EQ 0 THEN RETURN
  
  N_files = N_ELEMENTS(child)
  filelist = STRARR(N_files)
  
  FOR i=0,N_files-1 DO BEGIN
    uname = WIDGET_INFO(child[i], /uname)
    filename = ((*pState).FIDHASH EQ LONG(STRMID(uname, 12)))[0]
    filelist[i] = filename
  ENDFOR
  
END




PRO IDLgrTree::AddFile, file

  ;文件列表添加文件事件
  COMPILE_OPT idl2
  CATCH, error_status
  WIDGET_CONTROL, self.EVTOP, get_uvalue = pState
  
  R = QUERY_TIFF(file, info, geotiff = geotiff)
  
  IF ~R THEN tmp = DIALOG_MESSAGE('请选择正确的TIFF文件', /error)
  
  
  ;判断ID为Fid的文件是否在文件列表中打开，如果已经打开，则返回
  IF ((*pState).FIDHASH).HASKEY(file) EQ 1 THEN BEGIN
    Fid = ((*pState).FIDHASH)[file]
    child = WIDGET_INFO(((*pState).OTREE).ID, /all_children)
    IF child[0] NE 0 THEN BEGIN
      FOREACH element, child DO BEGIN
        uname = WIDGET_INFO(element, /uname)
        IF uname EQ 'file_branch_' + STRTRIM(STRING(Fid),2) THEN RETURN
      ENDFOREACH
    ENDIF
  ENDIF ELSE BEGIN
    ;将文件ID和路径保存为HASH
    (*pState).FIDIDX += 1
    (*pState).FIDHASH += HASH(file, (*pState).FIDIDX)
    Fid = (*pState).FIDIDX
  ENDELSE
  
  pos = STRPOS(file, '\', /reverse_search)
  filename = STRMID(file, pos+1)
  
  ;判断是单波段、多波段还是DEM图像
  IF info.CHANNELS LT 3 THEN BEGIN
    filebmp = *((*pState).ICONBMP[1])
    IF STRPOS(STRUPCASE(filename), 'DEM') NE -1 THEN filebmp = *((*pState).ICONBMP[2])
  ENDIF ELSE BEGIN
    filebmp = *((*pState).ICONBMP[0])
  ENDELSE
  
  ;添加文件列表
  file_branch = WIDGET_TREE(self.ID,  $
    value = filename,   $
    /folder, /top, /expanded,   $
    uname = 'file_branch_' + STRTRIM(STRING(Fid),2),  $
    bitmap = filebmp)
    
  ;添加波段列表，uname格式 “band_branch_2 5”，其中2是波段号，5是文件Fid
  FOR i = 0, info.CHANNELS-1 DO BEGIN
    band_leaf = WIDGET_TREE(file_branch,  $
      value = 'Band ' + STRTRIM(STRING(i+1),2),  $
      uname = 'band_branch_' + STRTRIM(STRING(i),2) + ' ' + STRTRIM(STRING(Fid),2), $
      bitmap = *((*pState).ICONBMP[3]) )
    IF i EQ 0 THEN WIDGET_CONTROL, band_leaf, /set_tree_select
  ENDFOR
  
  IF error_status THEN RETURN
  
  ;投影信息
  Proj = GET_GEOTIFF(file, *(*pState).GEOTIFF)
  
  IF Proj.PROJ EQ '' THEN RETURN
  IF SIZE(geotiff, /type) NE 8 THEN RETURN
  
  ;投影信息根节点
  proj_branch = WIDGET_TREE(file_branch,  $
    value = 'Map Info', $
    /folder, $
    bitmap = *((*pState).ICONBMP[4]))
    
  FOR i = 0, 4 DO BEGIN
    CASE i OF
      ;Proj
      0: BEGIN
        value = 'Proj: ' + Proj.PROJ
      END
      ;Pixel
      1: BEGIN
        value = 'Pixel: ' + Proj.PIXEL
      ENDCASE
      ;Datum
      2: BEGIN
        value = 'Datum: ' + Proj.DATUM
      END
      ;UL Geo
      3: BEGIN
        value = 'UL Geo: ' + Proj.UL_GEO
      END
      ;UL Map
      4: BEGIN
        value = 'UL Map: ' + Proj.UL_MAP
      END
      ELSE: value = ''
    ENDCASE
    
    proj_leaf = WIDGET_TREE(proj_branch, $
      value = value,  $
      bitmap = *((*pState).ICONBMP[3]))
  ENDFOR
  
END


PRO IDLgrTree::FoldFile, ev

  ;收起文件夹
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  file_ids = WIDGET_INFO(((*pState).OTREE).ID, /all_children)
  FOREACH element, file_ids DO WIDGET_CONTROL, element, set_tree_expanded = 0
  
END



PRO IDLgrTree::UnfoldFile, ev

  ;展开文件夹
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  file_ids = WIDGET_INFO(((*pState).OTREE).ID, /all_children)
  FOREACH element, file_ids DO WIDGET_CONTROL, element, /set_tree_expanded
  
END




PRO IDLgrTree::SetProperty, uname = uname,  $
    id = id,                                $
    evtop = evtop,                          $
    xsize = xsize,                          $
    ysize = ysize,                          $
    fidnow = fidnow,                        $
    FidRGB = FidRGB,                        $
    BandRGB_idx = BandRGB_idx,              $
    BandGray_idx = BandGray_idx,            $
    BandRGB_name = BandRGB_name,            $
    BandGray_name = BandGray_name
  ;
  IF N_ELEMENTS(uname) NE 0 THEN self.UNAME = uname
  IF N_ELEMENTS(id) NE 0 THEN self.ID = id
  IF N_ELEMENTS(evtop) NE 0 THEN self.EVTOP = evtop
  IF N_ELEMENTS(xsize) NE 0 THEN WIDGET_CONTROL, self.ID, xsize = xsize
  IF N_ELEMENTS(ysize) NE 0 THEN WIDGET_CONTROL, self.ID, ysize = ysize
  IF N_ELEMENTS(fidnow) NE 0 THEN self.FIDNOW = fidnow
  IF N_ELEMENTS(FidRGB) NE 0 THEN self.FIDRGB = FidRGB
  IF N_ELEMENTS(BandRGB_idx) NE 0 THEN self.BANDRGB_IDX = BandRGB_idx
  IF N_ELEMENTS(BandGray_idx) NE 0 THEN self.BANDGRAY_IDX = BandGray_idx
  IF N_ELEMENTS(BandRGB_name) NE 0 THEN self.BANDRGB_NAME = BandRGB_name
  IF N_ELEMENTS(BandGray_name) NE 0 THEN self.BANDGRAY_NAME = BandGray_name
  
END


PRO IDLgrTree::GetProperty, id = id,  $
    fidnow = fidnow,                  $
    FidRGB = FidRGB,                  $
    BandRGB_idx = BandRGB_idx,        $
    BandGray_idx = BandGray_idx,      $
    BandRGB_name = BandRGB_name,      $
    BandGray_name = BandGray_name
  ;
  id = self.ID
  fidnow = self.FIDNOW
  FidRGB = self.FIDRGB
  BandRGB_idx = self.BANDRGB_IDX
  BandGray_idx = self.BANDGRAY_IDX
  BandRGB_name = self.BANDRGB_NAME
  BandGray_name = self.BANDGRAY_NAME
  
END


PRO IDLgrTree::CLEANUP
  ;
  COMPILE_OPT IDL2
END



PRO IDLgrTree::Create

  ;搭建文件列表揭秘那

  wTree = WIDGET_TREE(self.PARENT, $
    event_pro = self.EVENT_PRO, $
    uname = self.UNAME,  $
    /CONTEXT_EVENTS )
    
  self.SETPROPERTY, id = wTree
  
  ;文件 - 右键菜单
  File_contextBase = WIDGET_BASE(self.PARENT, /context_menu, uname = 'File_contextMenu')
  
  Button = WIDGET_BUTTON(File_contextBase, $
    value = '关闭所选文件', $
    uname = "RemoveFile")
    
  Button = WIDGET_BUTTON(File_contextBase, $
    value = '收起所有列表', $
    uname = "FoldFile", /sep)
  Button = WIDGET_BUTTON(File_contextBase, $
    value = '放下所有列表', $
    uname = "UnfoldFile")
    
    
  ;波段 - 右键菜单
  Band_contextBase = WIDGET_BASE(self.PARENT, /context_menu, uname = 'Band_contextMenu')
  
  Button = WIDGET_BUTTON(Band_contextBase, $
    value = '加载所选波段', $
    uname = "LoadBand")
    
  Button = WIDGET_BUTTON(Band_contextBase, $
    value = '收起所有列表', $
    uname = "FoldFile", /sep)
  Button = WIDGET_BUTTON(Band_contextBase, $
    value = '放下所有列表', $
    uname = "UnfoldFile")
    
    
    
  ;空白 - 右键菜单
  Blank_contextBase = WIDGET_BASE(self.PARENT, /context_menu, uname = 'Blank_contextMenu')
  
  Button = WIDGET_BUTTON(Blank_contextBase, $
    value = '收起所有列表', $
    uname = "FoldFile")
  Button = WIDGET_BUTTON(Blank_contextBase, $
    value = '放下所有列表', $
    uname = "UnfoldFile")
    
    
END



FUNCTION IDLgrTree::INIT, parent = parent,  $
    xsize = xsize,                          $
    ysize = ysize,                          $
    uname = uname,                          $
    event_pro = event_pro
    
  self.PARENT = parent
  self.EVENT_PRO = event_pro
  IF N_ELEMENTS(xsize) NE 0 THEN self.XSIZE = xsize
  IF N_ELEMENTS(ysize) NE 0 THEN self.YSIZE = ysize
  IF N_ELEMENTS(uname) NE 0 THEN self.UNAME = uname
  
  self.CREATE
  
  RETURN, 1
  
END



PRO IDLGRTREE__DEFINE

  ;定义IDLgrTree对象
  structure = {IDLgrTree,     $
    evtop:0,                  $
    event_pro:'',             $   ;事件
    parent:0L,                $   ;父节点
    FidNow:0L,                $   ;存储当前文件ID
    FidRGB:[-1,-1,-1],        $   ;存储RGB三通道的文件ID
    BandRGB_idx:[-1,-1,-1],   $   ;存储RGB三通道的波段号
    BandRGB_name:['','',''],  $   ;存储RGB单通道波段名
    BandGray_idx:-1,          $   ;存储Gray模式下所选波段
    BandGray_name:'',         $   ;存储Gray模式下所选波段名
    ID:0L,                    $   ;Widget_Tree的ID
    xsize:0L,                 $   ;
    ysize:0L,                 $   ;
    uname:''}                     ;
    
END