PRO OVERVIEW::PanRect, ev
  ;处理鼠标事件的程序
  COMPILE_OPT idl2
  WIDGET_CONTROL, self.EVTOP, get_uvalue = pState
  
  IF (*pState).VIEWFID LT 1 THEN RETURN
  file = ((*pState).FIDHASH EQ (*pState).VIEWFID)[0]
  R = QUERY_TIFF(file, info)
  
  ;获取视图属性
  self.OVIEWEYE.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  self.OVIEWRECT.GETPROPERTY, data = RectData
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = view_vp, dimensions = view_vd
  
  oObjArr = (*pState).OWIN.SELECT(self.OVIEWEYE, [ev.X, ev.Y], DIMENSIONS=[10,10])
    
  PanIdx = 0
  IF OBJ_VALID(oObjArr[0]) THEN BEGIN
    FOR i=0,N_ELEMENTS(oObjArr)-1 DO BEGIN
    
      oObjArr[i].GETPROPERTY, name = name
      ;如果名称为'ViewRect'，则PanIdx设置为1
      IF name EQ 'ViewRect' THEN BEGIN
        PanIdx = 1
        CONTINUE
      ENDIF
    ENDFOR
  ENDIF
  
  ;根据PanIdx设置鼠标样式
  IF PanIdx THEN (*pState).OWIN.SETCURRENTCURSOR, 'move'  $
  ELSE (*pState).OWIN.SETCURRENTCURSOR, 'crosshair'
  
  CASE ev.TYPE OF
    0: BEGIN
      ;鼠标按下事件
      IF ev.PRESS EQ 1 AND PanIdx THEN BEGIN
        ;如果按下且PanIdx为1，则设置鼠标状态为EyePan
        self.MOUSESTATUS = 'EyePan'
        self.PANSTATUS = [1, ev.X, ev.Y]
        
      ENDIF ELSE IF ev.PRESS EQ 1 AND PanIdx EQ 0 THEN BEGIN
      
        ;如果按下且PanIdx为0，则处理图像平移
        xy = [ev.X-2, 200-(view_vd[1]-ev.Y-2)]*self.SCALE + [vp[0], vp[1]]
        
        rectCenter = (RectData[*,0]+RectData[*,2])/2
        
        distance = xy - RectCenter
        
        self.OVIEWRECT.SETPROPERTY, data = RectData + [[distance],[distance],[distance],[distance]]
        self.OVIEWMB.SETPROPERTY, data = RectData +         $
          [[distance],[distance],[distance],[distance]] -   $
          [[-self.SCALE,-self.SCALE],[-self.SCALE,self.SCALE],[self.SCALE,self.SCALE],[self.SCALE,-self.SCALE]]
          
        scale = view_vd[0]/view_vp[2]*MAX(info.DIMENSIONS)/200
        distance = -1*distance*scale/self.SCALE
        geoDis = [distance[0]*view_vp[2]/view_vd[0], distance[1]*view_vp[3]/view_vd[1]]
        
        view_vp[0:1] = view_vp[0:1] - geoDis
        
        ;设置IDLgrView的viewPlane_Rect属性
        (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = view_vp
        
        ;刷新显示
        IDLVIEWER_REFRESHDRAW, pState
        
      ENDIF
      
    END
    
    1: BEGIN
      IF self.MOUSESTATUS EQ 'EyePan' THEN BEGIN
      
        ;鼠标释放事件
        IF ev.RELEASE EQ 1 OR ev.RELEASE EQ 2 THEN BEGIN
          self.PANSTATUS = 0
          IDLVIEWER_REFRESHDRAW, pState
        ENDIF
      ENDIF
    END
    
    2: BEGIN
      ;鼠标移动事件
      IF self.MOUSESTATUS EQ 'EyePan' THEN BEGIN
        IF self.PANSTATUS[0] EQ 1 THEN BEGIN
          ;处理图像平移
          distance = [ev.X, ev.Y] - self.PANSTATUS[1:2]
          geoDis = [distance[0]*vp[2]/vd[0], distance[1]*vp[3]/vd[1]]
          
          self.OVIEWRECT.GETPROPERTY, data = data
          self.OVIEWRECT.SETPROPERTY, data = data + [[geoDis],[geoDis],[geoDis],[geoDis]]
          self.OVIEWMB.SETPROPERTY, data = data +     $
            [[geoDis],[geoDis],[geoDis],[geoDis]] -   $
            [[-self.SCALE,-self.SCALE],[-self.SCALE,self.SCALE],[self.SCALE,self.SCALE],[self.SCALE,-self.SCALE]]
            
          scale = view_vd[0]/view_vp[2]*MAX(info.DIMENSIONS)/200
          
          distance = -1*distance*scale
          
          geoDis = [distance[0]*view_vp[2]/view_vd[0], distance[1]*view_vp[3]/view_vd[1]]
          
          view_vp[0:1] = view_vp[0:1] - geoDis
          
          (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = view_vp
          
          self.PANSTATUS[1:2] = [ev.X, ev.Y]
          
          ;刷新显示
          IDLVIEWER_REFRESHDRAW, pState
        ENDIF
      ENDIF
    END
    ELSE:
  ENDCASE
  
END

PRO OVERVIEW::UpdateRect
  ;更新矩形框选区域的程序
  IF self.HIDE EQ 1 THEN RETURN
  
  WIDGET_CONTROL, self.EVTOP, get_uvalue = pState
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  
  ;计算坐标转换
  fid = (*pState).VIEWFID
  IF fid LT 1 THEN RETURN
  
  x = LONARR(4)
  y = LONARR(4)
  ev_X = [0,0,vd[0],vd[0]]
  ev_Y = [0,vd[1],vd[1],0]
  
  scale = vd[0]/vp[2]
  vp = vp*scale
  
  FOR i=0,3 DO BEGIN
    x[i] = ev_X[i]+vp[0]
    y[i] = ev_Y[i]+vp[1]
  ENDFOR
  
  x = FLOOR(x/scale)
  y = FLOOR(y/scale)
  
  ;更新矩形框选区域的数据
  self.OVIEWRECT.SETPROPERTY, data = TRANSPOSE([[x],[y]])
  
  x = CEIL(x - [-self.SCALE,-self.SCALE,self.SCALE,self.SCALE])
  y = CEIL(y - [-self.SCALE,self.SCALE,self.SCALE,-self.SCALE])
  
  ;更新模型数据
  self.OVIEWMB.SETPROPERTY, data = TRANSPOSE([[x],[y]])
  
  IDLVIEWER_REFRESHDRAW, pState
  
END

PRO OVERVIEW::LoadImage
  ;加载图像的程序
  WIDGET_CONTROL, self.EVTOP, get_uvalue = pState
  IF (*pState).VIEWFID LT 1 THEN RETURN
  
  ;获取文件信息
  fidnow = (*pState).VIEWFID
  file = ((*pState).FIDHASH EQ fidnow)[0]
  
  ;查询TIFF图像信息
  R = QUERY_TIFF(file, info)
  
  self.OVIEWEYE.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  
  IF vd[0]/vd[1] LE FLOAT((info.DIMENSIONS)[0])/(info.DIMENSIONS)[1] THEN BEGIN
    vp[0] = 0
    vp[1] = -(vd[1]*(info.DIMENSIONS)[0]/vd[0]-(info.DIMENSIONS)[1])/2
    vp[2:3] = [(info.DIMENSIONS)[0], vd[1]*(info.DIMENSIONS)[0]/vd[0]]
    
    self.SCALE = (info.DIMENSIONS)[0]/vd[0]
  ENDIF ELSE BEGIN
    vp[1] = 0
    vp[0] = -(vd[0]*(info.DIMENSIONS)[1]/vd[1]-(info.DIMENSIONS)[0])/2
    vp[2:3] = [vd[0]*(info.DIMENSIONS)[1]/vd[1], (info.DIMENSIONS)[1]]
    
    self.SCALE = (info.DIMENSIONS)[1]/vd[1]
  ENDELSE
  
  (*pState).OIMAGE.GETPROPERTY, data = data
  
  self.OVIEWEYE.SETPROPERTY, viewPlane_Rect = vp
  self.O