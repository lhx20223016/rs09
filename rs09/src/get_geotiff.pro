FUNCTION CONVERT_LATLON, input
  ;С����ת��Ϊ�ȷ���
  COMPILE_OPT idl2
  
  du = FIX(input)
  fen = FIX((input-du)*60)
  miao = ((input-du)*60-fen)*60
  
  RETURN, [du, fen, miao]
  
END



PRO LOC2MAP, file, Loc, geo_hash, outmap = outmap, lon_lat = lon_lat

  ;�����ļ������кţ��������;�γ��
  COMPILE_OPT idl2
  CATCH, error_status
  
  R = QUERY_TIFF(file, geotiff = geotiff)
  
  ;�ж��Ƿ�ΪTIFF�ļ�
  IF R NE 1 THEN BEGIN
    tmp = DIALOG_MESSAGE('Please select the tiff file', /error)
    RETURN
  ENDIF
  
  IF geo_hash.HASKEY(geotiff.PROJECTEDCSTYPEGEOKEY) THEN BEGIN
  
    ;���ͨ�� PROJECTEDCSTYPEGEOKEY ���Բ鵽ͶӰ��Ϣ
    Proj_datum = geo_hash[geotiff.PROJECTEDCSTYPEGEOKEY]
    
    PixelSize = (geotiff.MODELPIXELSCALETAG)[0:1]
    
    ;���Ͻ���Ԫ����
    OriMap = (geotiff.MODELTIEPOINTTAG)[3:4]
    
    ;�ɷֱ��ʺ����Ͻ��������������Ԫ������
    OutMap = [OriMap[0] + Loc[0]*PixelSize[0], OriMap[1] - Loc[1]*PixelSize[1]]
    
    ;����UL_Geo
    pos = STRPOS(Proj_datum[0], ' ', /reverse_search)
    Zone = STRMID(Proj_datum[0], pos+1)
    Tmp = STRMID(Zone, 0, /reverse_offset) EQ 'N' ? 1 : -1
    Zone = TEMPORARY(Tmp) * FIX(TEMPORARY(Zone))
    
    Map = MAP_PROJ_INIT('UTM', Zone = Zone)
    
    ;С����ʽ��γ��
    Tmp = MAP_PROJ_INVERSE(OutMap[0], OutMap[1], MAP_STRUCTURE = Map )
    
    ;ת��Ϊ�ȷ���
    UL_Geo = [CONVERT_LATLON(Tmp[0]), CONVERT_LATLON(Tmp[1])]
    ; �жϾ�γ������
    lon_idx = Tmp[0] LT 0 ? 'W' : 'E'
    lat_idx = Tmp[1] LT 0 ? 'S' : 'N'
    ul_geo_df = STRTRIM(STRING(ABS(FIX(UL_Geo))),2)
    
    Lon_Lat = ul_geo_df[0] + '��' +  $
      ul_geo_df[1] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[2])) + '"' + lon_idx + ', ' +  $
      ul_geo_df[3] + '��' +  $
      ul_geo_df[4] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[5])) + '"' + lat_idx
      
      
  ENDIF ELSE IF TOTAL(TAG_NAMES(geotiff) EQ 'GEOGCITATIONGEOKEY') EQ 1 THEN BEGIN
  
    ;���ͨ�� PROJECTEDCSTYPEGEOKEY ���ܲ鵽ͶӰ��Ϣ��ͨ����׼������뾭����⾭γ��
  
    PixelSize = (geotiff.MODELPIXELSCALETAG)[0:1]
    
    ;���Ͻ���Ԫ����
    OriMap = [(geotiff.MODELTIEPOINTTAG)[3] GT 1e7 ? LONG(STRMID(STRTRIM(STRING((geotiff.MODELTIEPOINTTAG)[3]),2), 2)) : (geotiff.MODELTIEPOINTTAG)[3], (geotiff.MODELTIEPOINTTAG)[4]]
    
    ;�ɷֱ��ʺ����Ͻ��������������Ԫ������
    OutMap = [OriMap[0] + Loc[0]*PixelSize[0], OriMap[1] - Loc[1]*PixelSize[1]]
    
    Map = MAP_PROJ_INIT('UTM', CENTER_LONGITUDE = geotiff.PROJCENTERLONGGEOKEY)
    
    ;����ת��ΪС����ʽ��γ��
    Tmp = MAP_PROJ_INVERSE(OutMap, MAP_STRUCTURE = Map )
    
    ;ת��Ϊ�ȷ���
    UL_Geo = [CONVERT_LATLON(Tmp[0]), CONVERT_LATLON(Tmp[1])]
    ; �жϾ�γ������
    lon_idx = Tmp[0] LT 0 ? 'W' : 'E'
    lat_idx = Tmp[1] LT 0 ? 'S' : 'N'
    ul_geo_df = STRTRIM(STRING(ABS(FIX(UL_Geo))),2)
    
    Lon_lat = ul_geo_df[0] + '��' +  $
      ul_geo_df[1] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[2])) + '"' + lon_idx + ', ' +  $
      ul_geo_df[3] + '��' +  $
      ul_geo_df[4] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[5])) + '"' + lat_idx
      
  ENDIF ELSE BEGIN
  
  END
  
  outmap = NUM_FORMATTER(outmap[0], Decimals = 2) + lon_idx + ', '  $
    + NUM_FORMATTER(outmap[1], Decimals = 2) + lat_idx
    
END




FUNCTION GET_GEOTIFF, file, geo_hash
  ;
  ;��ȡTIFF�ļ�ͶӰ��Ϣ
  ;
  ;���룺
  ;     file       --   TIFF�ļ�����·��
  ;     geo_hash   --   HASH(idx, ['proj','datum'])
  ;
  ;�����
  ;     structrue = {Proj:'' , Pixel:'' , Datum:'' , UL_Geo:'' , UL_Map:''}
  ;
  COMPILE_OPT idl2
  CATCH, error_status
  
  R = QUERY_TIFF(file, geotiff = geotiff)
  
  ;��ʼ������ֵResult
  Result = {Proj:'' , Pixel:'' , Datum:'' , UL_Geo:'' , UL_Map:''}
  
  ;�ж��Ƿ�ΪTIFF�ļ�
  IF R NE 1 THEN BEGIN
    tmp = DIALOG_MESSAGE('Please select the tiff file', /error)
    RETURN, Result
  ENDIF
  
  IF error_status NE 0 THEN RETURN, Result
  
  IF geo_hash.HASKEY(geotiff.PROJECTEDCSTYPEGEOKEY) THEN BEGIN
  
    ;���ͨ�� PROJECTEDCSTYPEGEOKEY ���Բ鵽ͶӰ��Ϣ
    Proj_datum = geo_hash[geotiff.PROJECTEDCSTYPEGEOKEY]
    
    Result.PROJ = Proj_datum[0]
    
    Result.PIXEL = NUM_FORMATTER(DOUBLE((geotiff.MODELPIXELSCALETAG)[0]), /delZero) + ' Meters'
    
    Result.DATUM = Proj_datum[1]
    
    Result.UL_MAP = NUM_FORMATTER((geotiff.MODELTIEPOINTTAG)[3], Decimals = 3) + ', '  $
      + NUM_FORMATTER((geotiff.MODELTIEPOINTTAG)[4], Decimals = 3)
      
    ;����UL_Geo
    pos = STRPOS(Proj_datum[0], ' ', /reverse_search)
    Zone = STRMID(Proj_datum[0], pos+1)
    Tmp = STRMID(Zone, 0, /reverse_offset) EQ 'N' ? 1 : -1
    Zone = TEMPORARY(Tmp) * FIX(TEMPORARY(Zone))
    
    Map = MAP_PROJ_INIT('UTM', Zone = Zone)
    
    ;С����ʽ��γ��
    Tmp = MAP_PROJ_INVERSE((geotiff.MODELTIEPOINTTAG)[3], $
      (geotiff.MODELTIEPOINTTAG)[4] , $
      MAP_STRUCTURE = Map )
      
    ;ת��Ϊ�ȷ���
    UL_Geo = [CONVERT_LATLON(Tmp[0]), CONVERT_LATLON(Tmp[1])]
    ; �жϾ�γ������
    lon_idx = Tmp[0] LT 0 ? 'W' : 'E'
    lat_idx = Tmp[1] LT 0 ? 'S' : 'N'
    ul_geo_df = STRTRIM(STRING(ABS(FIX(UL_Geo))),2)
    
    Result.UL_GEO = ul_geo_df[0] + '��' +  $
      ul_geo_df[1] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[2])) + '"' + lon_idx + ', ' +  $
      ul_geo_df[3] + '��' +  $
      ul_geo_df[4] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[5])) + '"' + lat_idx
      
      
  ENDIF ELSE IF TOTAL(TAG_NAMES(geotiff) EQ 'GEOGCITATIONGEOKEY') EQ 1 THEN BEGIN
  
    ;���ͨ�� PROJECTEDCSTYPEGEOKEY ���ܲ鵽ͶӰ��Ϣ��ͨ����׼������뾭����⾭γ��
    Result.PROJ = 'Transverse Mercator'
    Result.PIXEL = NUM_FORMATTER(DOUBLE((geotiff.MODELPIXELSCALETAG)[0]), /delZero) + ' Meters'
    Result.DATUM = geotiff.GEOGCITATIONGEOKEY
    Result.UL_MAP = NUM_FORMATTER((geotiff.MODELTIEPOINTTAG)[3], Decimals = 3) + ', '  $
      + NUM_FORMATTER((geotiff.MODELTIEPOINTTAG)[4], Decimals = 3)
      
    ul_map1 = (geotiff.MODELTIEPOINTTAG)[3] GT 1e7 ? LONG(STRMID(STRTRIM(STRING((geotiff.MODELTIEPOINTTAG)[3]),2), 2)) : (geotiff.MODELTIEPOINTTAG)[3]
    
    Map = MAP_PROJ_INIT('UTM', CENTER_LONGITUDE = geotiff.PROJCENTERLONGGEOKEY)
    
    ;С����ʽ��γ��
    Tmp = MAP_PROJ_INVERSE(ul_map1, (geotiff.MODELTIEPOINTTAG)[4] , MAP_STRUCTURE = Map )
    
    ;ת��Ϊ�ȷ���
    UL_Geo = [CONVERT_LATLON(Tmp[0]), CONVERT_LATLON(Tmp[1])]
    ; �жϾ�γ������
    lon_idx = Tmp[0] LT 0 ? 'W' : 'E'
    lat_idx = Tmp[1] LT 0 ? 'S' : 'N'
    ul_geo_df = STRTRIM(STRING(ABS(FIX(UL_Geo))),2)
    
    Result.UL_GEO = ul_geo_df[0] + '��' +  $
      ul_geo_df[1] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[2])) + '"' + lon_idx + ', ' +  $
      ul_geo_df[3] + '��' +  $
      ul_geo_df[4] + "'" + $
      NUM_FORMATTER(ABS((UL_Geo)[5])) + '"' + lat_idx
      
  ENDIF ELSE BEGIN
  ;    RETURN, null
  ENDELSE
  
  RETURN, Result
  
END






;��ע
;
;��ȡͶӰ�ļ���txt�ļ��ں��ж�ӦPROJECTEDCSTYPEGEOKEY��ͶӰ��Ϣ��
;  file = FILE_DIRNAME(ROUTINE_FILEPATH('IDLViewer')) + '\data\geotiff_temp.txt'
;  lines = file_lines(file)
;  OPENR, lun, file, /get_lun
;  data = STRARR(lines)
;  READF, lun, data
;  data = STRTRIM(data, 2)
;  FREE_LUN , lun
;
;  ;����ͶӰ��Ϣ��geotiff��ʽ����
;  geotiff = HASH(idx, ['proj','datum'])
;
;  FOR i = 0, lines-1 DO BEGIN
;    idx = LONG(STRMID(data[i], STRLEN(data[i])-5, 5))
;
;    pos1 = STRPOS(data[i], '_')
;    pos2 = STRPOS(data[i], ' ')
;
;    proj = STRMID(data[i], pos1+1, pos2-pos1)
;    proj = STRJOIN(STRSPLIT(proj, '_', /extract), ' ')
;    datum = STRMID(data[i], 0, pos1)
;
;    geotiff += HASH(idx, [proj, datum])
;
;  ENDFOR