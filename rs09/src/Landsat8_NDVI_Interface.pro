pro Landsat8_NDVI_Interface_event,event
  COMPILE_OPT idl2
  common a,pointer
  case tag_names(event,/structure_name) of
    "WIDGET_KILL_REQUEST" : BEGIN
      tmp=dialog_message("ȷ�Ϲر�",/question)
      if tmp eq "Yes" then begin
        widget_control,event.top,/destroy
        return
      endif
    end
    else: print,"others"
  endcase

  widget_control,event.id,get_uvalue=uvalue
  case uvalue of
    'adc' : begin
      inputfiles = DIALOG_PICKFILE(title='select files......',/multiple_files)


      (*pointer).inputfiles=inputfiles

      widget_control,(*pointer).img_txt,set_value=inputfiles

    end

    'shp' : begin
      shapefile=dialog_pickfile(title='select files......')
      if shapefile eq '' then begin
        mes2=dialog_message('No shape file has been selected!',/error)
        return
      endif

      (*pointer).shapefile=shapefile

      widget_control,(*pointer).dem_txt,set_value=shapefile

    end

    'save' : begin
      outname=dialog_pickfile(title='�������·��...',/directory)
      (*pointer).outname=outname
      widget_control,(*pointer).save_txt,set_value=outname
    end

    'confirm' : begin


      count=n_elements((*pointer).inputfiles)

      inputpath=file_dirname((*pointer).inputfiles[0])+'\'

      ;      outputpath=file_dirname((*pointer).outname)+'\'

      outputpath = (*pointer).outname

      print,outputpath

      for i=0,count-1 do begin
        inputfile=(*pointer).inputfiles[i]
        
        filebasename=file_basename(inputfile,'.tif')
        
        out_name = outputpath + filebasename + '_NDVI.dat'
        out_name1 = outputpath + filebasename + '_NDVI.tif'

        envi_open_file,inputfile,r_fid=fid_nor

        if (fid_nor eq -1) then begin

          envi_batch_exit

          return

        endif
        
        if(fid_nor eq -1)then return
        ENVI_FILE_QUERY,fid_nor,ns=ns_nor,nl=nl_nor,nb=nb_nor,dims=dims_nor
        map_info=envi_get_map_info(fid=fid_nor)
        pos_nor=lindgen(nb_nor)
        
        redBand = envi_get_data(fid=fid_nor,dims=dims_nor,pos=3)
        NirBand = envi_get_data(fid=fid_nor,dims=dims_nor,pos=4)

        NDVI = (NirBand/10000.0 - redBand/10000.0) / (NirBand/10000.0 + redBand/10000.0)

        waa=where(NDVI le -1)
        NDVI[waa]=-1
        wab=where(NDVI gt 1)
        NDVI[wab]=1
        
        res=where(finite(NDVI) eq 0,count)
        NDVI[res]=0

        ENVI_WRITE_ENVI_FILE,NDVI,out_name=out_name,map_info=map_info

        ENVI_FILE_MNG, ID = fid_nor, /REMOVE
        
        transftif, out_name, out_name1
        
        ;��Ԫ���ַ� ����ֲ�����Ƕ�   VFC=(NDVI-NDVImin)/(NDVImax-NDVImin)
        mina = 0.05
        maxa = 0.95

        wcc=where((NDVI gt -2),count)
        NDVIresultarr = fltarr(count)
        NDVIresultarr = NDVI[wcc]

        NDVIa=sort(NDVIresultarr)
        NDVIresultarr12 = NDVIresultarr[NDVIa]

        coutID1 = round(count*mina)
        coutID2 = round(count*maxa)

        NDVImin = NDVIresultarr12[coutID1-1]
        NDVImax = NDVIresultarr12[coutID2-1]

        ;  NDVImin = min(NDVI,/NAN)
        ;  NDVImax = max(NDVI,/NAN)

        VFC=(NDVI-NDVImin)/(NDVImax-NDVImin)

        ut_name = outputpath+'\'+filebasename+'_VFC.dat'
        ut_name1 = outputpath+'\'+filebasename+'_VFC.tif'
        ENVI_WRITE_ENVI_FILE,VFC,out_name=ut_name,map_info=map_info
        
        transftif, ut_name, ut_name1
        
        mes4=dialog_message('ֲ�����Ƕȼ������!',/info)
        
      endfor
      
      widget_control,event.top,/destroy

    end
  endcase
end

pro Landsat8_NDVI_Interface,ev

  ;Ӱ��ƴ�ӳ���
  ;�������

  COMPILE_OPT idl2
  ENVI,/restore_base_save_files
  ENVI_BATCH_INIT
  adctlb=WIDGET_BASE(title = "ֲ�����Ƕȼ�������", $
    xsize=600,ysize=400,/Tlb_Kill_Request_Events,$
    tlb_frame_attr=1)


  imgbase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=20)
  label1=widget_label(imgbase,value='ң��Ӱ��·��:',xoffset=5,yoffset=10)
  img_dir=widget_button(imgbase,value='ѡ��ң��Ӱ������',xoffset=120,yoffset=5,uvalue='adc')

  wlabel1=widget_label(imgbase,value='�ļ��б�',xoffset=240,yoffset=50)

  img_txt=widget_list(imgbase,xoffset=5,yoffset=80,xsize=80,ysize=12)


  ;  dembase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=236)
  ;  label2=widget_label(dembase,value='�о���������������·��',xoffset=200,yoffset=10)
  ;  demdir=widget_button(dembase,value="ѡ���о���������������",xoffset=5,yoffset=40,uvalue='shp')
  ;  dem_txt=widget_text(dembase,xsize=300,yoffset=40,xoffset=170)


  savebase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=320)
  savedir=widget_button(savebase,xoffset=5,yoffset=5,value='����ֲ�����ǶȽ��Ϊ:',uvalue='save')
  save_txt=widget_text(savebase,xsize=300,yoffset=6,xoffset=160)

  confirmbutton=widget_button(adctlb,xoffset=280,yoffset=360,value='ȷ��',xsize=70,ysize=35,uvalue='confirm')

  ;���ò�������ṹ��
  common a,pointer
  state = {adctlb:adctlb, $
    ;  tlb : tlb, $
    label1: label1, $
    ;    label2: label2, $
    ;    demdir: demdir, $
    ;    dem_txt: dem_txt, $
    wlabel1: wlabel1, $
    img_dir: img_dir, $
    img_txt : img_txt, $
    save_txt:save_txt,$

    ; map_info: create_struct(a,2),$
    outname:'',inputfiles:strarr(10),shapefile:''}
  pointer=ptr_new(/ALLOCATE_HEAP)
  *pointer=state


  widget_control,adctlb,/realize,/no_copy
  XMANAGER, 'Landsat8_NDVI_Interface', adctlb, /NO_BLOCK
end




