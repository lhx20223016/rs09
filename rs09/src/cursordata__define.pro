PRO CURSORDATA, ev

  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  tagName = TAG_NAMES(ev, /structure_name)
  

  IF tagName EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
    OBJ_DESTROY, (*pState).OCURSORDATA
    WIDGET_CONTROL, ev.TOP, /destroy
    
    CASE (*pState).VIEWIDX OF
      0: (*pState).OWIN.SETCURRENTCURSOR, 'Original'
      1: (*pState).OWIN.SETCURRENTCURSOR, 'Move'
      2: (*pState).OWIN.SETCURRENTCURSOR, 'Crosshair'
      ELSE:
    ENDCASE
    RETURN
  ENDIF
END



PRO CURSORDATA::SetValue, ev

  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  

  fid = (*pState).VIEWFID
  
  file = ((*pState).FIDHASH EQ fid)[0]
  ;  data = READ_TIFF(file, channels = 0)
  r = QUERY_TIFF(file, info)
  size = info.DIMENSIONS
  scale = vd[0]/vp[2]
  vp = vp*scale
  ;  size = SIZE(data, /dimensions)
  xy = [ev.X+vp[0], ev.Y+vp[1]]
  xy = xy/scale
  xy = FLOOR([xy[0], size[1]-xy[1]])
  
  ;  data = read_tiff(file, sub_rect = [xy[0],xy[1],1,1])
  ;
  ;  IF info.pixel_type EQ 1 THEN data = FIX(TEMPORARY(data))
  

  (*pState).OMODE.GETPROPERTY, index = index
  CASE index OF
  
    0: BEGIN
    
    
  
      Proj = GET_GEOTIFF(file, *(*pState).GEOTIFF)
      

      R = QUERY_TIFF(file, geotiff = geotiff)
      
      IF Proj.PROJ EQ '' THEN BEGIN
      

        IF xy[0] GE 0 AND xy[0] LT SIZE[0] AND xy[1] GE 0 AND xy[1] LT SIZE[1] THEN BEGIN
        
          channel = (*pState).VIEWBAND
          data = READ_TIFF(file, channels = channel, sub_rect = [xy[0],xy[1],1,1])
          data = data[0,0]
          IF info.PIXEL_TYPE EQ 1 THEN data = FIX(TEMPORARY(data))
          value = 'Location: (' + STRTRIM(STRING(xy[0]), 2) + ',' + STRTRIM(STRING(xy[1]), 2) + ')' + STRING(13B) + $
            'Data: ' + STRTRIM(STRING(data), 2)
            
        ENDIF ELSE BEGIN
          value = 'Location: (NaN,NaN)' + STRING(13B) + 'Data: NaN'
        ENDELSE
        
      ENDIF ELSE BEGIN
      
        IF xy[0] GE 0 AND xy[0] LT SIZE[0] AND xy[1] GE 0 AND xy[1] LT SIZE[1] THEN BEGIN
        
          channel = (*pState).VIEWBAND
          data = READ_TIFF(file, channels = channel, sub_rect = [xy[0],xy[1],1,1])
          data = data[0,0]
          IF info.PIXEL_TYPE EQ 1 THEN data = FIX(TEMPORARY(data))
          
          LOC2MAP, file, xy, *(*pState).GEOTIFF, outmap = outmap, lon_lat = lon_lat
          
          value = 'Proj: ' + proj.PROJ + STRING(13B) + $
            'Location: (' + STRTRIM(STRING(xy[0]), 2) + ',' + STRTRIM(STRING(xy[1]), 2) + ')' + STRING(13B) + $
            'Map: ' + outmap + STRING(13B) + $
            'Lon/Lat: ' + lon_lat + STRING(13B) + $
            'Data: ' + STRTRIM(STRING(data), 2)
            
        ENDIF ELSE BEGIN
          value = 'Proj: NaN' + STRING(13B) + $
            'Location: (NaN,NaN)' + STRING(13B) +  $
            'Map: NaN' + STRING(13B) + $
            'Lon/Lat: NaN' + STRING(13B) + $
            'Data: NaN'
        ENDELSE
        
        
      ENDELSE
    END
    
    1: BEGIN
      FidRGB = (*pState).VIEWFID_RGB
      BandRGB_idx = (*pState).VIEWBAND_RGB
      

      ns_rgb = [0L,0L,0L]
      nl_rgb = [0L,0L,0L]
      FOR i = 0,2 DO BEGIN

        file = ((*pState).FIDHASH EQ FidRGB[i])[0]
        R = QUERY_TIFF(file, info)
        ns_rgb[i] = info.DIMENSIONS[0]
        nl_rgb[i] = info.DIMENSIONS[1]
      ENDFOR
      
      IF TOTAL(ns_rgb EQ ns_rgb[0]) NE 3 OR TOTAL(nl_rgb EQ nl_rgb[0]) NE 3 THEN RETURN
      
      ;      dtype = info.pixel_type
      DN = MAKE_ARRAY(3, type = info.PIXEL_TYPE>2)
      
      
 
      Proj = GET_GEOTIFF(file, *(*pState).GEOTIFF)
      
      R = QUERY_TIFF(file, geotiff = geotiff)
      
      IF Proj.PROJ EQ '' THEN BEGIN
      
        IF xy[0] GE 0 AND xy[0] LT SIZE[0] AND xy[1] GE 0 AND xy[1] LT SIZE[1] THEN BEGIN
        
          FOR i=0,2 DO BEGIN
            file = ((*pState).FIDHASH EQ FidRGB[i])[0]
            DN[i] = READ_TIFF(file, channels = BandRGB_idx[i], sub_rect = [xy[0],xy[1],1,1])
          ENDFOR
          DN = STRTRIM(STRING(DN), 2)
          value = 'Location: (' + STRTRIM(STRING(xy[0]), 2) + ',' + STRTRIM(STRING(xy[1]), 2) + ')' + STRING(13B) + $
            'Data: R:' + DN[0] + ' G:' + DN[1] + ' B:' + DN[2]
            
        ENDIF ELSE BEGIN
          value = 'Location: (NaN,NaN)' + STRING(13B) + 'Data: NaN'
          
        ENDELSE
        
      ENDIF ELSE BEGIN
      

        IF xy[0] GE 0 AND xy[0] LT SIZE[0] AND xy[1] GE 0 AND xy[1] LT SIZE[1] THEN BEGIN
        
          LOC2MAP, file, xy, *(*pState).GEOTIFF, outmap = outmap, lon_lat = lon_lat

          FOR i=0,2 DO BEGIN
            file = ((*pState).FIDHASH EQ FidRGB[i])[0]
            DN[i] = READ_TIFF(file, channels = BandRGB_idx[i], sub_rect = [xy[0],xy[1],1,1])
          ENDFOR
          DN = STRTRIM(STRING(DN), 2)
          
          value = 'Proj: ' + proj.PROJ + STRING(13B) + $
            'Location: (' + STRTRIM(STRING(xy[0]), 2) + ',' + STRTRIM(STRING(xy[1]), 2) + ')' + STRING(13B) + $
            'Map: ' + outmap + STRING(13B) + $
            'Lon/Lat: ' + lon_lat + STRING(13B) + $
            'Data: R:' + DN[0] + ' G:' + DN[1] + ' B:' + DN[2]
            
        ENDIF ELSE BEGIN
          value = 'Proj: NaN' + STRING(13B) + $
            'Location: (NaN,NaN)' + STRING(13B) +  $
            'Map: NaN' + STRING(13B) + $
            'Lon/Lat: NaN' + STRING(13B) + $
            'Data: NaN'
        ENDELSE
        
        
      ENDELSE
      
    END
  ENDCASE
  
  WIDGET_CONTROL, self.LABEL_ID, set_value = value
  
END



PRO CURSORDATA::SetProperty, label_ID = Label_ID, wBase = wBase

  IF N_ELEMENTS(Label_ID) NE 0 THEN self.LABEL_ID = Label_ID
  IF N_ELEMENTS(wBase) NE 0 THEN self.WBASE = wBase
  
END




PRO CURSORDATA::GetProperty, parent = parent, wBase = wBase

  parent = self.PARENT
  wBase = self.WBASE
  
END




PRO CURSORDATA::CLEANUP
  ;
  COMPILE_OPT IDL2
END



PRO CURSORDATA::Create

  ;锟筋建锟斤拷锟斤拷
  COMPILE_OPT idl2
  WIDGET_CONTROL, self.PARENT, get_uvalue = pState
  
  wBase = WIDGET_BASE(group_leader = self.PARENT,   $
    /floating, title = '锟斤拷锟饺≈�',  $
    event_pro = 'cursorData',     $
    /tlb_kill_request_events)
    
  wLabel = WIDGET_LABEL(wBase, xsize = 300, ysize = 100, value = '', $
    xoffset = 5, yoffset = 5)
    
  WIDGET_CONTROL, wBase, /realize
  
  WIDGET_CONTROL, wBase, set_uvalue = pState
  
  self.SETPROPERTY, Label_ID = wLabel
  self.SETPROPERTY, wBase = wBase
  
END




FUNCTION CURSORDATA::INIT, parent = parent

  self.PARENT = parent
  self.CREATE
  
  RETURN, 1
  
END




PRO CURSORDATA__DEFINE

  COMPILE_OPT idl2
  
  structure = {CursorData,     $
    wBase:0,                   $
    parent:0,                  $   ;锟斤拷锟节碉拷锟斤拷锟�ID
    label_ID:0                 $   ;Widget_Label锟斤拷锟�ID
    }
    
END