pro transftif, mid_result, result
  compile_opt idl2
  envi, /restore_base_save_files
  envi_batch_init, log_file = 'batch.txt', /no_status_window
  
;  mid_result="C:\GFdata\data\GF1\31\GF1C_PMS_E112.3_N36.9_20190330_L1A1021390011\latlon.dat"
;  result="C:\GFdata\data\GF1\31\GF1C_PMS_E112.3_N36.9_20190330_L1A1021390011\1.tif"
  
  envi_open_file, mid_result, r_fid = out_fid
  envi_file_query, out_fid, dims = out_dims, nb = nb
  envi_output_to_external_format, fid = out_fid, dims = out_dims, pos = lindgen(nb), out_name = result, /tiff
  envi_file_mng, id = out_fid, /remove
end