pro Landsat8_Resize90_Interface_event, event
  COMPILE_OPT idl2
  common a, pointer
  case tag_names(event, /structure_name) of
    "WIDGET_KILL_REQUEST": begin
      tmp = dialog_message("确定要退出吗？", /question)
      if tmp eq "Yes" then begin
        widget_control, event.top, /destroy
        return
      endif
    end
    else: print, "others"
  endcase

  widget_control, event.id, get_uvalue = uvalue
  case uvalue of
    'adc': begin
      inputfiles = DIALOG_PICKFILE(title='选择文件......', /multiple_files)

      (*pointer).inputfiles = inputfiles

      widget_control, (*pointer).img_txt, set_value = inputfiles

    end

    'shp': begin
      shapefile = dialog_pickfile(title='选择文件......')
      if shapefile eq '' then begin
        mes2 = dialog_message('没有选择任何shape文件！', /error)
        return
      endif

      (*pointer).shapefile = shapefile

      widget_control, (*pointer).dem_txt, set_value = shapefile

    end

    'save': begin
      outname = dialog_pickfile(title='选择输出文件夹...', /directory)
      (*pointer).outname = outname
      widget_control, (*pointer).save_txt, set_value = outname
    end

    'confirm': begin

      count = n_elements((*pointer).inputfiles)

      inputpath = file_dirname((*pointer).inputfiles[0]) + '\'

      outputpath = (*pointer).outname

      print, outputpath

      for i = 0, count - 1 do begin
        inputfile = (*pointer).inputfiles[i]
        filebasename = file_basename(inputfile, '.tif')

        envi_open_file, inputfile, r_fid = fid

        if (fid eq -1) then begin

          envi_batch_exit

          return

        endif

        envi_file_query, fid, dims = dims, nb = nb

        pos = lindgen(nb)

        out_name = outputpath + filebasename + '_90.dat'
        
        out_name1 = outputpath + filebasename + '_90.tif'

        envi_doit, 'resize_doit', $

          fid = fid, pos = pos, dims = dims, $

          interp = 1, rfact = [3, 3], $

          out_name = out_name, r_fid = r_fid

        transftif, out_name, out_name1
        
        mes4 = dialog_message('90m数据已成功生成！', /info)

      endfor

      widget_control, event.top, /destroy

    end
  endcase
end

pro Landsat8_Resize90_Interface, ev
  ; 创建Landsat8 90m分辨率调整界面
  ; 初始化ENVI环境

  COMPILE_OPT idl2
  ENVI, /restore_base_save_files
  ENVI_BATCH_INIT
  adctlb = WIDGET_BASE(title = "Landsat8 90m分辨率调整工具", $
    xsize = 600, ysize = 400, /Tlb_Kill_Request_Events, $
    tlb_frame_attr = 1)

  imgbase = WIDGET_BASE(adctlb, frame = 3, xoffset = 0, yoffset = 20)
  label1 = widget_label(imgbase, value = '选择Landsat8影像文件夹:', xoffset = 5, yoffset = 10)
  img_dir = widget_button(imgbase, value = '选择Landsat8影像文件夹', xoffset = 120, yoffset = 5, uvalue = 'adc')

  wlabel1 = widget_label(imgbase, value = '文件列表：', xoffset = 240, yoffset = 50)

  img_txt = widget_list(imgbase, xoffset = 5, yoffset = 80, xsize = 80, ysize = 12)

  savebase = WIDGET_BASE(adctlb, frame = 3, xoffset = 0, yoffset = 320)
  savedir = widget_button(savebase, xoffset = 5, yoffset = 5, value = '选择输出文件夹为：', uvalue = 'save')
  save_txt = widget_text(savebase, xsize = 300, yoffset = 6, xoffset = 160)

  confirmbutton = widget_button(adctlb, xoffset = 280, yoffset = 360, value = '确认', xsize = 70, ysize = 35, uvalue = 'confirm')

  ; 创建共享结构体
  common a, pointer
  state = {adctlb: adctlb, $
    label1: label1, $
    wlabel1: wlabel1, $
    img_dir: img_dir, $
    img_txt: img_txt, $
    save_txt: save_txt, $

    outname: '', inputfiles: strarr(10), shapefile: ''}
  pointer = ptr_new(/ALLOCATE_HEAP)
  *pointer = state

  widget_control, adctlb, /realize, /no_copy
  XMANAGER, 'Landsat8_Resize90_Interface', adctlb, /NO_BLOCK
end