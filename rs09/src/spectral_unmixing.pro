FUNCTION HYPER2D, M
  ;输入三维图像，输出二维图像
  COMPILE_OPT idl2
  dims = SIZE(M, /dimensions)
  IF SIZE(M, /n_dimensions) NE 3 AND SIZE(M, /n_dimensions) NE 2 THEN BEGIN
    mytemp = DIALOG_MESSAGE('Input image must be 3D')
  ENDIF ELSE IF  SIZE(M, /n_dimensions) EQ 2 THEN BEGIN
    nb = 1
  ENDIF ELSE BEGIN
    nb = dims[2]
  ENDELSE
  
  ;矩阵转置
  M_temp = M
  FOR i = 0, nb-1 DO BEGIN
    M_temp[*,*,i] = TRANSPOSE(M[*,*,i])
  ENDFOR
  RETURN, REFORM(TEMPORARY(M_temp), dims[0]*dims[1], nb)
END

FUNCTION HYPER3D, M, ns, nl, nb
  ;ns-列数        nl-行数         nb-波段数
  ;输入二维图像，输出三维图像
  COMPILE_OPT idl2
  IF SIZE(M, /n_dimensions) NE 2 AND nb NE 1 THEN BEGIN
    mytemp = DIALOG_MESSAGE('Input image must be 2D')
  ENDIF ELSE BEGIN
    M_temp = FLTARR(ns, nl, nb)
    FOR i = 0, nb-1 DO BEGIN
      M_temp[*,*,i] = TRANSPOSE(REFORM(TRANSPOSE(M[*,i]), nl, ns))
    ENDFOR
    RETURN, TEMPORARY(M_temp)
  ENDELSE
END

FUNCTION INV, data
  ;求逆矩阵
  COMPILE_OPT idl2
  IF SIZE(data, /n_dimensions) LT 2 THEN BEGIN
    RETURN, 1/data
  ENDIF ELSE BEGIN
    RETURN, INVERT(data)
  ENDELSE
END

FUNCTION HYPERFCLS, M, U

  COMPILE_OPT idl2
  ;判断输入数据M和U是否均为2维
  IF SIZE(U, /n_dimensions) NE 2 THEN BEGIN
    mytemp = DIALOG_MESSAGE('U must be 2D data')
  ENDIF
  IF SIZE(M, /n_dimensions) NE 2 THEN BEGIN
    mytemp = DIALOG_MESSAGE('M must be 2D data', /error)
  ENDIF
  dims_M = SIZE(M, /dimensions)
  dims_U = SIZE(U, /dimensions)
  ;M 和 U 的行数相同，即波段数
  IF dims_M[1] NE dims_U[1] THEN BEGIN
    mytemp = DIALOG_MESSAGE('M and U must have the same number of spectral bands', /error)
  ENDIF
  p = dims_M[1] ;波段数
  N = dims_M[0] ;M的列数，图像的长乘宽
  q = dims_U[0]
  X = FLTARR(N, q)  ;初始化二维丰度图

  Mbckp = U
  
  FOR n1=0L,N-1 DO BEGIN
    count = q
    done = 0
    ref = INDGEN(q)
    r = M[n1,*]
    U = Mbckp
    WHILE ~done DO BEGIN
    
      als_hat = r#TRANSPOSE(U)#INV(U#TRANSPOSE(U))
      ones = INTARR(1,count)
      ones[*,*] = 1
      s = TRANSPOSE(ones)#INV(U#TRANSPOSE(U))
      afcls_hat = als_hat - (als_hat#TRANSPOSE(ones)-1) # (INV(ones#INV(U#TRANSPOSE(U))#TRANSPOSE(ones))) # TRANSPOSE(ones)#INV(U#TRANSPOSE(U))
      
      alpha = FLTARR(1,q)
      IF TOTAL(afcls_hat GT 0) EQ count THEN BEGIN
        alpha[ref] = afcls_hat
        BREAK
      ENDIF
      
      ;Multiply negative elements by their counterpart in the s vector.
      ;Find largest abs(a_ij, s_ij) and remove entry from alpha.
      idx = WHERE(afcls_hat LT 0)
      IF idx[0] LT 0 THEN idx = 0;!null = MIN(afcls_hat, idx)
      afcls_hat[idx] = afcls_hat[idx]/s[idx]
      maxIdx = (SORT(-ABS(afcls_hat[idx])))[0]
      maxIdx = idx[maxIdx]
      alpha[maxIdx] = 0
      ;size(U,2)   =    (size(U, /dimensions))[0]
      ;1:size(U,2)   =    indgen(1, (size(U, /dimensions))[0])
      idx_all = INDGEN(1, (SIZE(U, /dimensions))[0])
      keep = idx_all[WHERE(idx_all NE maxIdx)]
      U = U[keep, *]
      count = count - 1
      ref = ref[keep]
    ENDWHILE
    X[n1,*] = alpha
  ENDFOR
  
  RETURN, X
END

PRO SU_EVENT, ev
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = suState
  WIDGET_CONTROL, (*!ev).TOP, get_uvalue = pState
  uname = WIDGET_INFO(ev.ID, /uname)
  CASE uname OF
    ;选择端元文件按钮
    'ChooseEndm': BEGIN
      file = DIALOG_PICKFILE(path = (*pState).CURPATH,  $
        title = '选择端元文件',  $
        filter = '*.txt')
      WIDGET_CONTROL, (*suState).WENDMFILE, set_value = file
    END
    
    ;选择输出路径按钮
    'ChooseOut': BEGIN
      file = DIALOG_PICKFILE(path = (*pState).CURPATH,  $
        title = '选择输出文件名')
      WIDGET_CONTROL, (*suState).WOUTPATH, set_value = file
    END
    
    ;选择输入文件
    'Filelist': BEGIN
    END
    
    ;OK按钮事件，进行混合像元分解
    'OK': BEGIN
      selected = WIDGET_INFO((*suState).WFILELIST, /list_select)
      IF selected EQ -1 THEN RETURN
      
      ;获取输入文件
      Infile = (*suState).FILELIST[selected]
      
      ;获取端元文件
      WIDGET_CONTROL, (*suState).WENDMFILE, get_value = EndmFile
      IF ~FILE_TEST(EndmFile) THEN RETURN
      ;获取端元数（列）和波段数（行）
      OPENR, lun, EndmFile, /get_lun
      ;获取波段数
      bands = FILE_LINES(EndmFile)
      
      ;读第一行
      tmp = ''
      READF, lun, tmp
      
      ;获取端元数
      endm_num = N_ELEMENTS(STRSPLIT(tmp, /extract))
      
      endm = FINDGEN(endm_num, bands)
      FREE_LUN, lun
      
      ;将端元数据读入endm
      OPENR, lun, EndmFile, /get_lun
      READF, lun, endm
      FREE_LUN, lun
      
      ;获取输出路径
      WIDGET_CONTROL, (*suState).WOUTPATH, get_value = OutFile
      
      ;获取数据
      r = QUERY_TIFF(inFile, info)
      data = READ_TIFF(inFile, INTERLEAVE = 2)
      
      ;混合像元分解
      FOR i = 1,10 DO BEGIN
        IF WIDGET_INFO((*suState).PRSBAR, /valid) THEN BEGIN
          IDLPROGRESSBAR_SETVALUE, (*suState).PRSBAR, i*10
        ENDIF ELSE BEGIN
          RETURN
        ENDELSE
        WAIT, 0.1
        IF i EQ 4 THEN data_X2d = HYPERFCLS(TEMPORARY(HYPER2D(data)), endm)
      ENDFOR
      
      WIDGET_CONTROL, ev.TOP, /destroy
      
      ;转化为三维
      data_X3d = HYPER3D(TEMPORARY(data_X2d), info.DIMENSIONS[0], info.DIMENSIONS[1], endm_num)
      
      ;保存结果
      WRITE_TIFF, OutFile, TEMPORARY(TRANSPOSE(data_X3d, [2,0,1])), /float
      
      ;将结果文件添加到文件列表
      (*pState).OTREE.ADDFILE, OutFile
      
    END
    
    ;取消按钮事件
    'Cancel': BEGIN
      WIDGET_CONTROL, ev.TOP, /destroy
    END
  ENDCASE
END

PRO SPECTRAL_UNMIXING, ev

  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  ;搭建混合像元分解界面
  wSUBase = WIDGET_BASE(group_leader = ev.TOP,   $
    title = '混合像元分解',                       $
    /column,                                     $
    tlb_frame_attr = 1,                          $
    /modal)
    
  ;获取已打开文件列表
  (*pState).OTREE.GETFILELIST, FileList = FileList
  
  ;已打开文件列表
  wFilebase = WIDGET_BASE(wSUBase, /column, /frame, ypad = 2)
  wLabel = WIDGET_LABEL(wFilebase, value = '选择输入文件', /align_left, ysize = 20)
  wFilelist = WIDGET_LIST(wFilebase, value = FileList, xsize = 60, ysize = 5, uname = 'Filelist')
  
  ;选择端元文件
  wEndmBase = WIDGET_BASE(wSUBase, row=2, /frame)
  wLabel = WIDGET_LABEL(wEndmBase, value = '选择端元数据           ')
  wButton = WIDGET_BUTTON(wEndmBase, value = '选择', uname = 'ChooseEndm')
  wEndmFile = WIDGET_TEXT(wEndmBase, value = '',xsize = 62)
  
  ;选择输出路径
  wPathBase = WIDGET_BASE(wSUBase, row=2, /frame)
  wLabel = WIDGET_LABEL(wPathBase, value = '选择输出路径           ')
  wButton = WIDGET_BUTTON(wPathBase, value = '选择', uname = 'ChooseOut')
  wOutPath = WIDGET_TEXT(wPathBase, value = '',xsize = 62)
  
  ;进度条和按钮
  wButtonBase = WIDGET_BASE(wSUBase, column = 3)
  wOK = WIDGET_BUTTON(wButtonBase, value = '确定', uname = 'OK', xsize = 60, ysize = 25)
  wCancel = WIDGET_BUTTON(wButtonBase, value = '取消', uname = 'Cancel', xsize = 60, ysize = 25)
  prsbar = IDLPROGRESSBAR(wButtonBase)
  
  WIDGET_CONTROL, wSUBase, /realize
  
  ;居中显示混合像元分解界面
  CENTERTLB, wSUBase
  
  IF N_ELEMENTS(Filelist) EQ 0 THEN Filelist = ''
  
  state = {Filelist:Filelist,     $
    wFilelist:wFilelist,          $
    wEndmFile:wEndmFile,          $
    wOutPath:wOutPath,            $
    PrsBar:PrsBar,                $
    ev:ev}
    
  WIDGET_CONTROL, wSUBase, set_uvalue = PTR_NEW(state, /no_copy)
  
  XMANAGER, 'SU', wSUBase, /no_block
END