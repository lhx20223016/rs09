;+
; :NAME: IDL Viewer
;
; :DESCRIPTION:
;   - Support TIFF fromat data
;   - Open, View, Band Management, ZoomIn, ZoomOut, Pan, et al.
;
; :AUTHOR: duhj@esrichina.com.cn
;
; :VERSION: 1.1
;-


PRO IDLVIEWER_EVENT, ev

  ;主界面事件
  COMPILE_OPT idl2
  DEFSYSV, '!ev', PTR_NEW(ev)
  
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState

  (*pState).OTREE.SETPROPERTY, evtop = ev.TOP
  
  tagName = TAG_NAMES(ev, /structure_name)
  
  ;关闭程序事件
  IF tagName EQ 'WIDGET_KILL_REQUEST' THEN BEGIN
    ret = DIALOG_MESSAGE('确定退出IDL Viewer吗？', title = 'IDL Viewer', /question)
    IF ret EQ 'No' THEN RETURN
    WIDGET_CONTROL, ev.TOP, /destroy
    PTR_FREE, !ev
    RETURN
  ENDIF
  
  uname = WIDGET_INFO(ev.ID, /uname)
  
  CASE uname OF
    ;
    'tlb': BEGIN
      IDLVIEWER_RESIZE, ev
      
    END
    
    ;滚轮和平移事件
    'wDraw': BEGIN
      IF (*pState).VIEWFID LT 1 THEN RETURN
      
      IF ev.TYPE EQ 4 THEN IDLVIEWER_REFRESHDRAW, pState
      IF ev.TYPE EQ 7 THEN IDLVIEWER_WHEEL, ev
      IDLVIEWER_PAN, ev
      
      ;鹰眼图内矩形框平移事件
      oViewArr = (*pState).OWIN.SELECT((*pState).OSCENE, $
        [ev.X, ev.Y], DIMENSIONS=[10,10] )
        
      IF OBJ_VALID((*pState).OEYE) THEN BEGIN
        oEyeArr = (*pState).OWIN.SELECT((*pState).OEYE.OVIEWEYE, $
          [ev.X, ev.Y], DIMENSIONS=[10,10] )
      ENDIF
      
      ;如果鼠标在鹰眼图上
      IF N_ELEMENTS(oViewArr) EQ 3 AND OBJ_VALID((*pState).OEYE) THEN BEGIN
        (*pState).OEYE.PANRECT, ev
      ENDIF ELSE IF (*pState).MOUSESTATUS NE 'Pan' THEN BEGIN
        CASE (*pState).VIEWIDX OF
          0: (*pState).OWIN.SETCURRENTCURSOR, 'original'
          1: (*pState).OWIN.SETCURRENTCURSOR, 'move'
          2: (*pState).OWIN.SETCURRENTCURSOR, 'crosshair'
        ENDCASE
      ENDIF
      
      ;如果鼠标位于鹰眼图上，则不可鼠标取值
      IF N_ELEMENTS(oViewArr) EQ 3 THEN RETURN
      
      ;拉框放大事件
      IDLVIEWER_ZOOMIN, ev
      
      ;双击显示数据
      (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
      IF ev.PRESS EQ 1 AND ev.CLICKS EQ 2 THEN BEGIN
      
        CASE OBJ_VALID((*pState).OCURSORDATA) OF
          0: BEGIN
            IF (*pState).VIEWIDX EQ 0 THEN BEGIN
              oCursorData = OBJ_NEW('CursorData', parent = ev.TOP)
              (*pState).OCURSORDATA = oCursorData
              (*pState).OWIN.SETCURRENTCURSOR, 'CROSSHAIR'
            ENDIF
          END
          1: BEGIN
            (*pState).OCURSORDATA.GETPROPERTY, wBase = wBase
            WIDGET_CONTROL, wBase, /destroy
            OBJ_DESTROY, (*pState).OCURSORDATA
            CASE (*pState).VIEWIDX OF
              0: (*pState).OWIN.SETCURRENTCURSOR, 'Original'
              1: (*pState).OWIN.SETCURRENTCURSOR, 'Move'
              2: (*pState).OWIN.SETCURRENTCURSOR, 'Crosshair'
              ELSE:
            ENDCASE
          END
        ENDCASE
      ENDIF
      
      ;如果对象有效，则对弹出的“鼠标取值”对话框进行取值
      IF OBJ_VALID((*pState).OCURSORDATA) EQ 1 THEN BEGIN
        (*pState).OWIN.SETCURRENTCURSOR, 'CROSSHAIR'
        (*pState).OCURSORDATA.SETVALUE, ev
      END
    END
    
    ;按钮Load事件
    'Load': (*pState).OMODE.HANDLEEVENT, ev
    
    ;工具栏选择按钮事件
    'Select': BEGIN
      (*pState).VIEWIDX = 0
      ;设置鼠标样式
      (*pState).OWIN.SETCURRENTCURSOR, 'original'
      
      ;设置按钮效果
      IDLVIEWER_BNTSTYLE, ev, 1
      
    END
    
    ;工具栏平移按钮事件
    'Hand': BEGIN
      (*pState).VIEWIDX = 1
      (*pState).OWIN.SETCURRENTCURSOR, 'move'
      
      ;设置按钮效果
      IDLVIEWER_BNTSTYLE, ev, 2
    END
    
    ;拉框放大
    'Zoom': BEGIN
    
      (*pState).VIEWIDX = 2
      (*pState).OWIN.SETCURRENTCURSOR, 'crosshair'
      
      ;设置按钮效果
      IDLVIEWER_BNTSTYLE, ev, 3
    END
    
    ;工具栏放大缩小事件
    'Zoomin': IDLVIEWER_ZOOM, ev, 0    ; 0为放大
    'Zoomout': IDLVIEWER_ZOOM, ev, 1   ; 1为缩小
    
    ;工具栏重置视图按钮事件
    'ResetView': BEGIN
    
      ;调整viewPlane_Rect，居中显示原始大小图像
      IF (*pState).VIEWFID LT 1 THEN RETURN
      
      file = ((*pState).FIDHASH EQ (*pState).VIEWFID)[0]
      
      R = QUERY_TIFF(file, info)
      
      (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
      
      vp = INTARR(4)
      vp[0] = -(vd[0]-(info.DIMENSIONS)[0])/2
      vp[1] = -(vd[1]-(info.DIMENSIONS)[1])/2
      vp[2:3] = vd[0:1]
      
      (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
      
      ;更新鹰眼图
      IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.UPDATERECT
      
      IDLVIEWER_REFRESHDRAW, pState
      
    END
    
    ;工具栏FitWin按钮事件
    'FitWindow': BEGIN
    
      ;调整图像适应窗口大小
      IF (*pState).VIEWFID LT 1 THEN RETURN
      file = ((*pState).FIDHASH EQ (*pState).VIEWFID)[0]
      R = QUERY_TIFF(file, info)
      
      (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
      
      IF vd[0]/vd[1] LE FLOAT((info.DIMENSIONS)[0])/(info.DIMENSIONS)[1] THEN BEGIN
        vp[0] = 0
        vp[1] = -(vd[1]*(info.DIMENSIONS)[0]/vd[0]-(info.DIMENSIONS)[1])/2
        vp[2:3] = [(info.DIMENSIONS)[0], vd[1]*(info.DIMENSIONS)[0]/vd[0]]
        
      ENDIF ELSE BEGIN
        vp[1] = 0
        vp[0] = -(vd[0]*(info.DIMENSIONS)[1]/vd[1]-(info.DIMENSIONS)[0])/2
        vp[2:3] = [vd[0]*(info.DIMENSIONS)[1]/vd[1], (info.DIMENSIONS)[1]]
        
      ENDELSE
      
      (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
      
      ;更新鹰眼图
      IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.UPDATERECT
      
      IDLVIEWER_REFRESHDRAW, pState
      
    END
    
    ;显示鹰眼图
    'Overview': BEGIN
    
      IF OBJ_VALID((*pState).OEYE) EQ 0 THEN BEGIN
        (*pState).OEYE = OBJ_NEW('OverView', evtop = ev.TOP)
        (*pState).OEYE.UPDATERECT
        RETURN
      ENDIF
      
      ;隐藏和打开鹰眼图
      (*pState).OEYE.GETPROPERTY, hide = hide
      CASE hide OF
        0: BEGIN
          (*pState).OEYE.SETPROPERTY, hide = 1
        END
        1: BEGIN
          (*pState).OEYE.SETPROPERTY, hide = 0
        END
      ENDCASE
      
    END
    
    ;R/G/B三个单选按钮事件
    'R': IF ev.SELECT EQ 1 THEN (*pState).OMODE.SETPROPERTY, RGB_IDX = 0
    'G': IF ev.SELECT EQ 1 THEN (*pState).OMODE.SETPROPERTY, RGB_IDX = 1
    'B': IF ev.SELECT EQ 1 THEN (*pState).OMODE.SETPROPERTY, RGB_IDX = 2
    
    ;右键移除文件事件
    'RemoveFile': (*pState).OTREE.REMOVEFILE, ev
    
    ;移除所有文件事件
    'CloseAllFiles': (*pState).OTREE.CLOSEALLFILES, ev
    
    ;退出事件
    'Exit': BEGIN
      ret = DIALOG_MESSAGE('确定退出IDL Viewer吗？', title = 'IDL Viewer', /question)
      IF ret EQ 'No' THEN RETURN
      WIDGET_CONTROL, ev.TOP, /destroy
      RETURN
    END
    
    ;右键收起文件夹事件
    'FoldFile': (*pState).OTREE.FOLDFILE, ev
    
    ;右键放下文件夹事件
    'UnfoldFile': (*pState).OTREE.UNFOLDFILE, ev
    
    ;右键加载波段事件
    'LoadBand': (*pState).OTREE.LOADBAND, ev
    
    ;Gray Scale单选按钮事件
    'Gray': BEGIN
      IF ev.SELECT EQ 1 THEN BEGIN
        (*pState).OMODE.CHANGE2GRAY, ev
      ;        (*pState).OMODE.SETPROPERTY, RGB_IDX = 0
      ENDIF
    END
    
    ;RGB Color单选按钮事件
    'RGB': BEGIN
      IF ev.SELECT EQ 1 THEN BEGIN
        (*pState).OMODE.CHANGE2RGB, ev
      ENDIF
    END
    
    ;打开文件事件，支持多选
    'Open': BEGIN
      file = DIALOG_PICKFILE(path = (*pState).CURPATH,  $
        get_path = newPath,                             $
        filter = '*',                               $
        /MULTIPLE_FILES)
      (*pState).CURPATH = newPath
      
      FOR i=0,N_ELEMENTS(file)-1 DO BEGIN
        IDLVIEWER_OPEN, STRJOIN(STRSPLIT(file[i], '\', /extract), '\'), multi = N_ELEMENTS(file)-1-i
      ENDFOR
      
      ;刷新显示
      IDLVIEWER_REFRESHDRAW, pState
    END
    
    ;关于事件
    'About': BEGIN
      oAbout = OBJ_NEW('IDLgrAbout',  $
        parent = ev.TOP,  $
        xsize = 482,  $
        ysize = 294,  $
        logoFile = (*pState).IMAGEPATH + '\IDLViewer_About.jpg',  $
        title = '关于IDL Viewer')
    END
    
    ;打开帮助文档
    'Help': BEGIN
      SPAWN, FILE_DIRNAME(ROUTINE_FILEPATH('IDLViewer')) + '\Resource\IDLViewer帮助文档.pdf', /hide
    END
    ELSE: RETURN
  ENDCASE
  
END




PRO IDLVIEWER_OTREE, ev
  ;
  ;oTree对象事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  ;右键菜单事件
  (*pState).OTREE.HANDLEEVENT, ev
  
  IF WIDGET_INFO(ev.ID, /tree_select) EQ -1 THEN RETURN
  
  WIDGET_CONTROL, ev.ID, get_value = bandname
  uname = WIDGET_INFO(ev.ID, /uname)
  IF uname EQ 'wTree' OR uname EQ '' THEN RETURN
  
  file_or_band = STRMID(uname, 0, 11)
  
  ;判断选中的是文件还是波段
  IF  file_or_band EQ 'file_branch' THEN BEGIN
  
    ;选中文件，复制Fidnow并返回
    fidnow = STRMID(uname, 12)
    (*pState).OTREE.SETPROPERTY, fidnow = LONG(fidnow)
    RETURN
    
  ENDIF ELSE BEGIN
  
    ;选中波段
    uname_tmp = STRSPLIT(uname, /extract)
    
    fidnow = LONG(uname_tmp[1])
    Bandnow = LONG(STRMID(uname_tmp[0], 12))
    
    ;设置oTree属性
    (*pState).OTREE.SETPROPERTY, fidnow = LONG(fidnow)
    
    (*pState).OMODE.GETPROPERTY, RGB_IDX = RGB_IDX
    (*pState).OTREE.GETPROPERTY, BandRGB_IDX = bandold
    (*pState).OTREE.GETPROPERTY, FidRGB = fidold
    
    bandold[RGB_IDX] = Bandnow
    fidold[RGB_IDX] = fidnow
    
    (*pState).OTREE.SETPROPERTY, BandRGB_IDX = bandold
    (*pState).OTREE.SETPROPERTY, FidRGB = fidold
    
    (*pState).OTREE.SETPROPERTY, BandGray_idx = bandnow
    
    ;双击显示单波段
    IF ev.CLICKS EQ 2 THEN BEGIN
      (*pState).OTREE.LOADBAND, ev
      (*pState).VIEWFID = fidnow
      (*pState).VIEWBAND = bandnow
      
      ;鹰眼图事件
      IF OBJ_VALID((*pState).OEYE) THEN BEGIN
        (*pState).OEYE.SETPROPERTY, clear = 0
        (*pState).OEYE.LOADIMAGE
        (*pState).OEYE.UPDATERECT
      ENDIF
    ENDIF
    
  ENDELSE
  
  (*pState).OMODE.GETPROPERTY, RGB_IDX = RGB_IDX
  
  file = ((*pState).FIDHASH EQ FIDNOW)[0]
  pos = STRPOS(file, '\', /reverse_search)
  filename = STRMID(file, pos+1)
  
  R = QUERY_TIFF(file, info)
  
  ;显示DIMS
  Type_Name = ['UNDEFINED', 'BYTE', 'INT', 'LONG', 'FLOAT', 'DOUBLE', 'COMPLEX', 'STRING', 'STRUCT', 'DCOMPLEX', 'POINTER', 'OBJREF', 'UINT', 'ULONG', 'LONG64', 'ULONG64']
  dims = STRTRIM(STRING(info.DIMENSIONS[0]),2) + '×' + STRTRIM(STRING(info.DIMENSIONS[1]),2)
  (*pState).OMODE.GETPROPERTY, DIMSTEXT = DIMSTEXT
  WIDGET_CONTROL, DIMSTEXT, set_value = TEMPORARY(dims) + ' (' + Type_Name[info.PIXEL_TYPE] + ') ' + '[BSQ]'
  
  
  ;设置所选波段名称
  (*pState).OMODE.GETPROPERTY, INDEX = INDEX
  IF INDEX EQ 1 THEN BEGIN
  
    ; RGB模式下
    (*pState).OMODE.GETPROPERTY, RGB_IDX = RGB_IDX
    (*pState).OMODE.GETPROPERTY, RGB_BAND = RGB_BAND
    WIDGET_CONTROL, RGB_BAND[RGB_IDX], set_value = bandname + ':' + filename
    
    (*pState).OTREE.GETPROPERTY, BANDRGB_NAME = BANDRGB_NAME
    BANDRGB_NAME[RGB_IDX] = bandname + ':' + filename
    (*pState).OTREE.SETPROPERTY, BANDRGB_NAME = BANDRGB_NAME
    (*pState).OTREE.SETPROPERTY, BANDGRAY_NAME = bandname + ':' + filename
    
    RGB_IDX += 1
    IF RGB_IDX GT 2 THEN RGB_IDX = 0
    WIDGET_CONTROL, RGB_BAND[RGB_IDX+3], /set_button
    (*pState).OMODE.SETPROPERTY, RGB_IDX = RGB_IDX
    
  ENDIF ELSE BEGIN
  
    ; Gray模式下
    (*pState).OMODE.GETPROPERTY, GRAY_BAND = GRAY_BAND
    WIDGET_CONTROL, GRAY_BAND, set_value = bandname + ':' + filename
    (*pState).OTREE.SETPROPERTY, BANDGRAY_NAME = bandname + ':' + filename
    
    (*pState).OTREE.GETPROPERTY, BANDRGB_NAME = BANDRGB_NAME
    BANDRGB_NAME[RGB_IDX] = bandname + ':' + filename
    (*pState).OTREE.SETPROPERTY, BANDRGB_NAME = BANDRGB_NAME
    
    RGB_IDX += 1
    IF RGB_IDX GT 2 THEN RGB_IDX = 0
    (*pState).OMODE.SETPROPERTY, RGB_IDX = RGB_IDX
    
  ENDELSE
  
END






PRO IDLVIEWER_OPEN, file, multi = multi

  ;文件打开事件
  COMPILE_OPT idl2
  ev = *(!ev)
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  IF ~FILE_TEST(file) THEN RETURN
  IF ~QUERY_TIFF(file) THEN BEGIN
    ;
    tmp = DIALOG_MESSAGE('请选择正确的TIFF文件', /info)
    RETURN
  ENDIF
  
  ;判断FidHash的key中是否存在file
  IF ((*pState).FIDHASH).HASKEY(file) EQ 1 THEN BEGIN
    tmp = DIALOG_MESSAGE('文件已打开', /info)
    RETURN
  ENDIF
  
  ;将文件ID和路径保存为HASH
  (*pState).FIDIDX += 1
  (*pState).FIDHASH += HASH(file, (*pState).FIDIDX)
  
  pos = STRPOS(file, '\', /reverse_search)
  filename = STRMID(file, pos+1)
  
  R = QUERY_TIFF(file, info)
  
  ;文件列表添加新打开文件名和波段名
  (*pState).OTREE.ADDFILE, file
  
  ;判断是否打开了多个文件
  IF N_ELEMENTS(multi) NE 0 THEN BEGIN
    IF MULTI NE 0 THEN RETURN
  ENDIF
  
  ; 创建数组存储数据
  channels = info.CHANNELS GE 3 ? INDGEN(3) : 0
  index = info.CHANNELS GE 3 ? 3 : 2
  
  data = REVERSE(READ_TIFF(file, channels = REVERSE(channels)), index)
  
  ;获取显示数据，默认加载 RGB = Band 3/2/1 （如果是全色图像，则只加载Band 1）
  IF info.CHANNELS GE 3 THEN BEGIN
  
    ;设置显示模式为RGB
    (*pState).OMODE.CHANGE2RGB, ev, file = file
    
    (*pState).OMODE.GETPROPERTY, RGB_Button = RGB_Button
    (*pState).OMODE.GETPROPERTY, RGB_Band = RGB_Band
    WIDGET_CONTROL, RGB_Button, /set_button
    WIDGET_CONTROL, RGB_Band[3], /set_button
    
    ;设置RGB三个波段名显示
    BANDRGB_NAME = STRARR(3)
    FOR i = 0, 2 DO BEGIN
      WIDGET_CONTROL, RGB_Band[i], set_value = 'Band ' +  $
        STRTRIM(STRING(2-i+1), 2) + ':' + filename
      BANDRGB_NAME[i] = 'Band ' + STRTRIM(STRING(2-i+1), 2) + ':' + filename
    ENDFOR
    
    ;设置oTree属性
    (*pState).OTREE.SETPROPERTY, BANDRGB_NAME = TEMPORARY(BANDRGB_NAME)
    (*pState).OMODE.SETPROPERTY, RGB_IDX = 0
    (*pState).OTREE.SETPROPERTY, FidRGB = [(*pState).FIDIDX,(*pState).FIDIDX,(*pState).FIDIDX]
    (*pState).OTREE.SETPROPERTY, BANDRGB_IDX = [2,1,0]
    (*pState).OTREE.SETPROPERTY, BANDGRAY_idx = 0
    
    (*pState).VIEWFID = (*pState).FIDIDX
    (*pState).VIEWBAND = 0
    (*pState).VIEWFID_RGB = [(*pState).FIDIDX,(*pState).FIDIDX,(*pState).FIDIDX]
    (*pState).VIEWBAND_RGB = [2,1,0]
    
  ENDIF ELSE BEGIN
  
    ;设置显示模式为Gray
    (*pState).OMODE.CHANGE2GRAY, ev
    
    ;设置显示波段名
    (*pState).OMODE.GETPROPERTY, GRAY_BAND = GRAY_BAND
    (*pState).OMODE.GETPROPERTY,GRAY_BUTTON = GRAY_BUTTON
    WIDGET_CONTROL, GRAY_BAND, set_value = 'Band 1'+ ':' + filename
    WIDGET_CONTROL, GRAY_BUTTON, /set_button
    ;
    ;设置oTree属性
    (*pState).OTREE.SETPROPERTY, FidRGB = [(*pState).FIDIDX,-1,-1]
    (*pState).OTREE.SETPROPERTY, BANDRGB_IDX = [0, -1, -1]
    (*pState).OTREE.SETPROPERTY, BANDRGB_NAME = ['Band 1'+ ':' + filename, '', '']
    (*pState).OTREE.SETPROPERTY, BANDGRAY_idx = 0
    
    (*pState).VIEWFID = (*pState).FIDIDX
    (*pState).VIEWBAND = 0
    (*pState).VIEWFID_RGB = [(*pState).FIDIDX,-1,-1]
    (*pState).VIEWBAND_RGB = [0, -1, -1]
    
  ENDELSE
  
  (*pState).OTREE.SETPROPERTY, BANDGRAY_NAME = 'Band 1'+ ':' + filename
  (*pState).OTREE.SETPROPERTY, Fidnow = ((*pState).FIDHASH)[file]
  
  ;显示DIMS
  dims = STRTRIM(STRING(info.DIMENSIONS[0]),2) + '×' + STRTRIM(STRING(info.DIMENSIONS[1]),2)
  (*pState).OMODE.GETPROPERTY, DIMSTEXT = DIMSTEXT
  WIDGET_CONTROL, DIMSTEXT, set_value = TEMPORARY(dims) + ' (' + TYPENAME(data) + ') ' + '[BSQ]'
  
  
  
  ;设置oTree的Fidnow属性
  (*pState).OTREE.SETPROPERTY, fidnow = ((*pState).FIDHASH)[file]
  
  ;设置按钮效果
  IF (*pState).VIEWIDX EQ 0 THEN BEGIN
    FOREACH element, (*pState).TOOLGROUP DO WIDGET_CONTROL, element, /SENSITIVE
    IDLVIEWER_BNTSTYLE, ev, 1
  ENDIF
  
  ;把数据set到oImage的data属性
  (*pState).OIMAGE.SETPROPERTY, data = LINEAR2(TEMPORARY(data))
  ;  (*pState).VIEWFID = ((*pState).FIDHASH)[file]
  
  ;调整viewPlane_Rect，居中显示原始大小图像
  drawSizeALL = WIDGET_INFO((*pState).WDRAW, /geom)
  drawSize = [drawSizeALL.XSIZE, drawSizeALL.YSIZE]
  vp = INTARR(4)
  vp[0] = -(drawSize[0]-(info.DIMENSIONS)[0])/2
  vp[1] = -(drawSize[1]-(info.DIMENSIONS)[1])/2
  vp[2:3] = drawSize[0:1]
  
  (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
  
  ;更新鹰眼图
  IF OBJ_VALID((*pState).OEYE) THEN BEGIN
    (*pState).OEYE.SETPROPERTY, clear = 0
    (*pState).OEYE.LOADIMAGE
    (*pState).OEYE.UPDATERECT
  ENDIF
  
END




PRO IDLVIEWER_WHEEL, ev
  ;
  ;滚轮事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  
  ;缩放系数
  tmpScale = 1. - FLOAT(ev.CLICKS)/10
  
  ;当前鼠标在View中的位置
  oriLoc = [vp[0] + DOUBLE(ev.X)*vp[2]/vd[0],   $
    vp[1] + DOUBLE(ev.Y)*vp[3]/vd[1]]
    
  ;缩放后View显示区域
  vp[2:3] = vp[2:3]*tmpScale
  distance = (oriLoc - vp[0:1])*tmpScale
  vp[0:1] = oriLoc - distance
  
  ;设置显示范围
  (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
  
  ;更新鹰眼图内矩形框属性
  IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.UPDATERECT
  
  ;刷新显示
  IDLVIEWER_REFRESHDRAW, pState
  
END



PRO IDLVIEWER_ZOOM, ev, Zoomidx

  ;放大缩小事件
  ;Zoomidx： 0 - 放大， 1 - 缩小
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  
  ;缩放系数
  CASE Zoomidx OF
    0: tmpScale = 1. - 1./10
    1: tmpScale = 1. + 1./10
  ENDCASE
  ;当前鼠标在View中的位置
  oriLoc = [vp[0] + (vd[0]/2)*vp[2]/vd[0],   $
    vp[1] + (vd[1]/2)*vp[3]/vd[1]]
  ;
  ;缩放后View显示区域
  vp[2:3] = vp[2:3]*tmpScale
  distance = (oriLoc - vp[0:1])*tmpScale
  vp[0:1] = oriLoc - distance
  
  ;设置显示范围
  (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
  
  ;更新鹰眼图
  IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.UPDATERECT
  
  ;刷新显示
  IDLVIEWER_REFRESHDRAW, pState
  
END




PRO IDLVIEWER_ZOOMIN, ev

  ;拉框放大事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  oViewArr = (*pState).OWIN.SELECT((*pState).OSCENE, $
    [ev.X, ev.Y], DIMENSIONS=[10,10])
    
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  
  CASE ev.TYPE OF
    0: BEGIN
      ;左键按下
      IF ev.PRESS EQ 1 AND (*pState).VIEWIDX EQ 2 AND N_ELEMENTS(oViewArr) NE 3 THEN BEGIN
        ;左键按下，为拉框放大做准备
        (*pState).MOUSESTATUS = 'Zoom'
        (*pState).PANSTATUS = [2, ev.X, ev.Y]
      ENDIF
      
      ;鼠标中间取消放大
      IF ev.PRESS EQ 2 THEN (*pState).OPOLYGON.SETPROPERTY, hide = 1
      
    END
    
    1: BEGIN
      IF (*pState).MOUSESTATUS EQ 'Zoom' THEN BEGIN
      
        ;左键释放
        IF ev.RELEASE EQ 1 THEN BEGIN
          (*pState).PANSTATUS = 0
          (*pState).OPOLYGON.SETPROPERTY, hide = 1
          
          (*pState).OPOLYGON.GETPROPERTY, data = data
          
          IF N_ELEMENTS(data) EQ 0 THEN RETURN
          
          x1 = data[0,0]
          y1 = data[1,0]
          x2 = data[0,2]
          y2 = data[1,2]
          data = !null
          x_len = ABS(x1-x2)
          y_len = ABS(y1-y2)
          
          x_start = x1<x2
          y_start = y1<y2
          
          ;根据拉框长宽比与显示区域的比较，设置oView的viewPlane_Rect属性
          IF FLOAT(x_len)/y_len GE FLOAT(vd[0])/vd[1] THEN BEGIN
            y_tmp = FLOAT(x_len)*vd[1]/vd[0]
            y_start = y_start - (y_tmp-y_len)/2.
            scale = FLOAT(x_len)/vp[2]
            (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = [x_start, y_start, vp[2]*scale, vp[3]*scale]
          ENDIF ELSE BEGIN
            x_tmp = FLOAT(y_len)*vd[0]/vd[1]
            x_start = x_start - (x_tmp-x_len)/2.
            scale = FLOAT(y_len)/vp[3]
            (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = [x_start, y_start, vp[2]*scale, vp[3]*scale]
          ENDELSE
          
          OBJ_DESTROY, (*pState).OPOLYGON
          (*pState).OPOLYGON = OBJ_NEW('IDLgrPolygon')
          (*pState).OMODEL.ADD, (*pState).OPOLYGON
          
          ;更新鹰眼图内矩形框属性
          IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.UPDATERECT
          
          IDLVIEWER_REFRESHDRAW, pState
          
        ENDIF
      ENDIF
    END
    
    2: BEGIN
      ;鼠标移动
      IF (*pState).MOUSESTATUS EQ 'Zoom' THEN BEGIN
        IF (*pState).PANSTATUS[0] EQ 2 THEN BEGIN
        
          ;画框
          scale = vd[0]/vp[2]
          vp = vp*scale
          x1 = (*pState).PANSTATUS[1] + vp[0]
          y1 = (*pState).PANSTATUS[2] + vp[1]
          x2 = ev.X + vp[0]
          y2 = ev.Y + vp[1]
          
          ;显示Polygon并设置其Data属性
          (*pState).OPOLYGON.SETPROPERTY, hide = 0
          (*pState).OPOLYGON.SETPROPERTY, data = [[x1,y1],[x1,y2],[x2,y2],[x2,y1]]/scale, style = 1, color = [255,0,0]
          
          ;更新显示
          IDLVIEWER_REFRESHDRAW, pState
        ENDIF
      ENDIF
    END
    ELSE:
  ENDCASE
  
END




PRO IDLVIEWER_PAN, ev

  ;平移事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp, dimensions = vd
  
  oViewArr = (*pState).OWIN.SELECT((*pState).OSCENE, $
    [ev.X, ev.Y], DIMENSIONS=[10,10])
    
  CASE ev.TYPE OF
    0: BEGIN
      ;左键按下
      IF ev.PRESS EQ 1 AND (*pState).VIEWIDX EQ 1 AND N_ELEMENTS(oViewArr) NE 3 THEN BEGIN
        ;左键按下，为平移做准备
        (*pState).MOUSESTATUS = 'Pan'
        (*pState).PANSTATUS = [1, ev.X, ev.Y]
      ENDIF
      
      ;中键按下
      IF ev.PRESS EQ 2 AND N_ELEMENTS(oViewArr) NE 3 THEN BEGIN
        ;中键按下，为平移做准备
        (*pState).MOUSESTATUS = 'Pan'
        (*pState).OWIN.SETCURRENTCURSOR, 'move'
        (*pState).PANSTATUS = [1, ev.X, ev.Y]
      ENDIF
      
    END
    
    1: BEGIN
      IF (*pState).MOUSESTATUS EQ 'Pan' THEN BEGIN
      
        ;左键或中键释放
        IF ev.RELEASE EQ 1 OR ev.RELEASE EQ 2 THEN BEGIN
          (*pState).PANSTATUS = 0
          IDLVIEWER_REFRESHDRAW, pState
          
          (*pState).MOUSESTATUS = ''
          
          IF (*pState).VIEWIDX EQ 0 THEN (*pState).OWIN.SETCURRENTCURSOR, 'original'
          IF (*pState).VIEWIDX EQ 2 THEN (*pState).OWIN.SETCURRENTCURSOR, 'crosshair'
          
          OBJ_DESTROY, (*pState).OPOLYGON
          (*pState).OPOLYGON = OBJ_NEW('IDLgrPolygon', hide = 1)
          (*pState).OMODEL.ADD, (*pState).OPOLYGON
        ENDIF
      ENDIF
    END
    
    2: BEGIN
      ;鼠标移动
      IF (*pState).MOUSESTATUS EQ 'Pan' THEN BEGIN
        IF (*pState).PANSTATUS[0] EQ 1 THEN BEGIN
          ;移动视图
          distance = [ev.X, ev.Y] - (*pState).PANSTATUS[1:2]
          geoDis = [distance[0]*vp[2]/vd[0], distance[1]*vp[3]/vd[1]]
          
          vp[0:1] = vp[0:1] - geoDis
          (*pState).PANSTATUS[1:2] = [ev.X, ev.Y]
          ;
          (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
          (*pState).OWIN.SETCURRENTCURSOR, 'Move'
          
          IF OBJ_VALID((*pState).OEYE) THEN (*pState).OEYE.UPDATERECT
          
          ;更新显示
          IDLVIEWER_REFRESHDRAW, pState
        ENDIF
      ENDIF
    END
    ELSE:
  ENDCASE
  
END



PRO IDLVIEWER_BNTSTYLE, ev, idx

  ;按钮样式事件
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  FOR i=1,3 DO BEGIN
    WIDGET_CONTROL, (*pState).TOOLGROUP[i], set_value = (*pState).TOOLBMP[i*2-1], /bitmap
  ENDFOR
  IF idx EQ 0 THEN BEGIN
    FOREACH element, (*pState).TOOLGROUP DO WIDGET_CONTROL, element, SENSITIVE = 0
    WIDGET_CONTROL, (*pState).TOOLGROUP[0], SENSITIVE = 1
    RETURN
  ENDIF
  WIDGET_CONTROL, (*pState).TOOLGROUP[idx], set_value = (*pState).TOOLBMP[idx*2], /bitmap
  
END



PRO IDLVIEWER_RESIZE, ev

  ;随窗体大小调整显示界面
  COMPILE_OPT idl2
  WIDGET_CONTROL, ev.TOP, get_uvalue = pState
  
  baseSize = [ev.X > 600,  ev.Y>400]
  
  ;获取wDraw原始大小
  info = WIDGET_INFO((*pState).WDRAW, /geom)
  oriSize = [info.XSIZE, info.YSIZE]
  
  ;获取新的wDraw大小
  newSize = [baseSize[0]-(*pState).OFFSETXY[0], baseSize[1]-(*pState).OFFSETXY[1]]
  
  ;适应调整后尺寸
  (*pState).OVIEW.GETPROPERTY, viewPlane_Rect = vp
  xCenter = vp[0] + vp[2]/2
  yCenter = vp[1] + vp[3]/2
  vp[2:3] = vp[2:3]*newSize/oriSize
  vp[0:1] = [xCenter, yCenter] - vp[2:3]/2
  
  (*pState).OVIEW.SETPROPERTY, viewPlane_Rect = vp
  (*pState).OVIEW.SETPROPERTY, dimension = newSize
  
  WIDGET_CONTROL, (*pState).WDRAW, xsize = newSize[0], ysize = newSize[1]
  (*pState).OTREE.SETPROPERTY, ysize = newSize[1] - 177
  
  ;更新鹰眼图属性
  IF OBJ_VALID((*pState).OEYE) THEN BEGIN
    (*pState).OEYE.REFRESH
    (*pState).OEYE.UPDATERECT
  ENDIF
  
  ;刷新显示
  IDLVIEWER_REFRESHDRAW, pState
END





PRO IDLVIEWER_REFRESHDRAW, pState

  ;刷新显示
  (*pState).OWIN.DRAW, (*pState).OSCENE
END





PRO IDLVIEWER
  ;
  ;主程序

  COMPILE_OPT idl2
  
  CD, current = rootdir
  DEVICE, get_screen_size = screenSize
  
  ;图片资源路径
  ImagePath = FILE_DIRNAME(ROUTINE_FILEPATH('IDLViewer')) + '\resource\bitmap'
  
  ;启动界面
  ScreenImage = ImagePath + '\IDLViewer_Splash.jpg'
  data = READ_IMAGE(ScreenImage)
  SplashBase = SHOW_SPLASH_SCREEN(data, /true)
  
  ;搭建界面
  tlb = WIDGET_BASE(title = '基于Landsat8的植被覆盖度计算系统',     $
    mbar = mbar,                              $
    uname = 'tlb',                            $
    /tlb_kill_request_events,                 $
    /tlb_size_events,                         $
    /column,                                  $
    map = 0,                                  $   ;隐藏
    event_pro = 'tlb_event')
    
  ;菜单栏 - File
  wFile = WIDGET_BUTTON(mbar, /menu, value = '文件')
  wOpen = WIDGET_BUTTON(wFile,  value = '打开', uname = 'Open')
  wClose = WIDGET_BUTTON(wFile, value = '关闭所有文件', uname = 'CloseAllFiles', /sep)
  wExit = WIDGET_BUTTON(wFile, value = '退出', uname = 'Exit', /sep)
  
  
  ;*********************添加自定义菜单 - 示例代码***************************
  wtemp = WIDGET_BUTTON(mbar, value = '图像基本信息统计', /menu)
  wButton = WIDGET_BUTTON(wtemp, value = '信息统计',event_pro = 'Landsat8_Stats_Interface')
  ;**********************************************************************
  
  ;*********************添加自定义菜单 - **********************************
  wtemp = WIDGET_BUTTON(mbar, value = '图像增强', /menu)
  wButton = WIDGET_BUTTON(wtemp, value = '2%拉伸',event_pro = 'Landsat8_Linear_Interface')
  ;**********************************************************************
  
  ;*********************添加自定义菜单 - **********************************
  wtemp = WIDGET_BUTTON(mbar, value = '裁剪', /menu)
  wButton = WIDGET_BUTTON(wtemp, value = '矢量文件裁剪', event_pro = 'Clip_Interface')
  ;**********************************************************************


  ;*********************添加自定义菜单 - **********************************
  wtemp = WIDGET_BUTTON(mbar, value = '影像预处理', /menu)
  wButton = WIDGET_BUTTON(wtemp, value = '预处理',event_pro = 'Landsat8_Resize60_Interface')
  ;**********************************************************************

  ;*********************添加自定义菜单 - **********************************
  wtemp = WIDGET_BUTTON(mbar, value = '植被覆盖度的计算', /menu)
  wButton = WIDGET_BUTTON(wtemp, value = '植被覆盖度',event_pro = 'Landsat8_NDVI_Interface')
;  wButton = WIDGET_BUTTON(wtemp, value = '水体指数', event_pro = 'Landsat8_MNDWI_Interface')
;  wButton = WIDGET_BUTTON(wtemp, value = '居民地', event_pro = 'Landsat8_NBI_Interface')
  ;**********************************************************************

;  ;*********************添加自定义菜单 - **********************************
;  wtemp = WIDGET_BUTTON(mbar, value = '不同地表类型提取', /menu)
;  wButton = WIDGET_BUTTON(wtemp, value = '使用植被指数阈值掩膜提取植被',event_pro = 'Landsat8_NDVIV_Interface')
;  wButton = WIDGET_BUTTON(wtemp, value = '使用水体指数阈值掩膜提取水体', event_pro = 'Landsat8_MNDWIV_Interface')
;  wButton = WIDGET_BUTTON(wtemp, value = '使用居民地指数阈值掩膜提取居民地', event_pro = 'Landsat8_NBIV_Interface')
;  ;**********************************************************************
  
  
  
  ;菜单栏 - Help
  wHelp = WIDGET_BUTTON(mbar, /menu, value = '帮助')
  wHelpBtn = WIDGET_BUTTON(wHelp, value = '帮助', uname = 'Help')
  wAbout = WIDGET_BUTTON(wHelp, value = '关于IDL Viewer', uname = 'About')
  
  ;工具栏图标
  Toolbmp = ImagePath + ['\open.bmp',      $
    '\select.bmp', '\selected.bmp',        $
    '\hand.bmp', '\handed.bmp',            $
    '\zoom.bmp', '\zoomed.bmp',            $
    '\zoom_in.bmp', '\zoom_out.bmp',       $
    '\reset.bmp', '\fitwindow.bmp',        $
    '\eye.bmp']
    
  ;工具栏
  wToolbar = WIDGET_BASE(tlb, /row)
  wOpenTool = WIDGET_BUTTON(wToolbar, value = Toolbmp[0], uname = 'Open', /bitmap, /flat, tooltip = '打开文件')
  wBlank = WIDGET_LABEL(wToolbar, value = ' ')
  wSelect = WIDGET_BUTTON(wToolbar, value = Toolbmp[1], uname = 'Select', /bitmap, /flat, SENSITIVE = 0, tooltip = '选择')
  wHand = WIDGET_BUTTON(wToolbar, value = Toolbmp[3], uname = 'Hand', /bitmap, /flat, SENSITIVE = 0, tooltip = '平移')
  wZoom = WIDGET_BUTTON(wToolbar, value = Toolbmp[5], uname = 'Zoom', /bitmap, /flat, SENSITIVE = 0, tooltip = '拉框放大')
  wZoomIn = WIDGET_BUTTON(wToolbar, value = Toolbmp[7], uname = 'Zoomin', /bitmap, /flat, SENSITIVE = 0, tooltip = '放大')
  wZoomOut = WIDGET_BUTTON(wToolbar, value = Toolbmp[8], uname = 'Zoomout', /bitmap, /flat, SENSITIVE = 0, tooltip = '缩小')
  wReset = WIDGET_BUTTON(wToolbar, value = Toolbmp[9], uname = 'ResetView', /bitmap, /flat, SENSITIVE = 0, tooltip = '重置')
  wFitWin = WIDGET_BUTTON(wToolbar, value = Toolbmp[10], uname = 'FitWindow', /bitmap, /flat, SENSITIVE = 0, tooltip = '适应窗口')
  wEye = WIDGET_BUTTON(wToolbar, value = Toolbmp[11], uname = 'Overview', /bitmap, /flat, SENSITIVE = 0, tooltip = '显示鹰眼图')
  
  ToolGroup = [wOpenTool, wSelect, wHand, wZoom, wZoomin, wZoomout, wReset, wFitWin, wEye]
  
  BaseSize = screenSize*2/3
  BaseSize[1] -= screenSize[1]/10
  
  wBase = WIDGET_BASE(tlb, /row)
  
  ;文件列表 - IDLgrTree
  wTreeBase = WIDGET_BASE(wBase, /column, /frame)
  oTree = OBJ_NEW('IDLgrTree',      $
    parent = wTreeBase,             $
    uname = 'wTree',                $
    ysize = BaseSize[1]-177,        $
    event_pro = 'IDLViewer_oTree')    
    
  ;显示模式 - DisplayMode
  oMode = OBJ_NEW('DisplayMode', parent = wTreeBase)
  
  ;绘图区 - IDLgrDraw
  wDrawBase = WIDGET_BASE(wBase, /column, /frame)
  wDraw = WIDGET_DRAW(wDrawBase, graphics_level = 2,    $
    xsize = BaseSize[0]-250,                            $
    ysize = BaseSize[1],                                $
    retain = 0,                                         $
    uname = 'wDraw',                                    $
    /expose_events,                                     $
    /button_events,                                     $
    /wheel_events,                                      $
    /motion_events)
    
  DrawSize = [BaseSize[0]-250, BaseSize[1]]
  
  ;读取投影文件
  file = FILE_DIRNAME(ROUTINE_FILEPATH('IDLViewer')) + '\resource\geotiff.txt'
  lines = FILE_LINES(file)
  OPENR, lun, file, /get_lun
  data = STRARR(lines)
  READF, lun, data
  data = STRTRIM(data, 2)
  FREE_LUN , lun
  
  ;保存投影信息，geotiff格式如下
  geotiff = HASH(0L, ['proj','datum'])
  
  FOR i = 0, lines-1 DO BEGIN
    idx = LONG(STRMID(data[i], STRLEN(data[i])-5, 5))
    
    pos1 = STRPOS(data[i], '_')
    pos2 = STRPOS(data[i], ' ')
    
    proj = STRMID(data[i], pos1+1, pos2-pos1)
    proj = STRTRIM(STRJOIN(STRSPLIT(proj, '_', /extract), ' '), 2)
    datum = STRMID(data[i], 0, pos1)
    
    geotiff += HASH(idx, [proj, datum])
    
  ENDFOR
  DELVAR, data
  
  ;读取波段列表的图标（多光谱图像、全色、DEM、波段、投影）
  iconPath = ImagePath + ['\img.bmp', '\gray.bmp', '\dem.bmp', '\band.bmp', '\proj.bmp']
  iconBmp = PTRARR(5)
  FOR i = 0,4 DO BEGIN
    iconBmp[i] = PTR_NEW(TRANSPOSE(READ_BMP(iconPath[i], /rgb), [1,2,0]), /no_copy)
  ENDFOR
  
  WAIT, 1
  WIDGET_CONTROL, SplashBase, /destroy
  WIDGET_CONTROL, tlb, /realize
  
  ;控制主程序显示在屏幕中间
  CENTERTLB, tlb
  
  ;显示主界面
  WIDGET_CONTROL, tlb, /map
  
  geom = WIDGET_INFO(tlb, /Geometry)
  offsetXY = [geom.XSIZE, geom.YSIZE] - Drawsize
  
  WIDGET_CONTROL, wDraw, get_value = oWin
  oWin.SETCURRENTCURSOR, 'original'
  
  ;IDLgrScene对象定义，用来显示图像、鹰眼图等
  oScene = OBJ_NEW('IDLgrScene')
  
  ;拉框放大的多边形对象
  oPolygon = OBJ_NEW('IDLgrPolygon')
  
  oView = OBJ_NEW('IDLgrView',  $
    color = [255,255,255],      $
    dimensions = DrawSize)
    
  oModel = OBJ_NEW('IDLgrModel')
  oImage = OBJ_NEW('IDLgrImage')
  
  oScene.ADD, oView
  oView.ADD, oModel
  oModel.ADD, oImage
  oModel.ADD, oPolygon
  
  oWin.DRAW, oView
  
  pState = PTR_NEW({                        $
    oWin:oWin,                              $
    oScene:oScene,                          $
    oTree:oTree,                            $  ;文件列表自定义对象
    oMode:oMode,                            $
    oView:oView,                            $
    oModel:oModel,                          $
    oImage:oImage,                          $
    oPolygon:oPolygon,                      $  ;拉框放大功能中的多边形
    oEye:OBJ_NEW(),                         $  ;鹰眼图对象
    WDRAW:WDRAW,                            $
    panStatus:DBLARR(3),                    $
    mouseStatus:'',                         $
    offsetXY:offsetXY,                      $
    FidIdx:0L,                              $  ;以TIFF文件打开顺序建立索引，作为file id
    FidHash:HASH(),                         $  ;使用HASH保存对应FidIdx的文件路径,格式为['FilePath', FidIdx], FidIdx为0表示未打开
    GeoTiff:PTR_NEW(geotiff, /no_copy),     $
    viewFid:0L,                             $  ;显示区域中显示文件ID
    viewFid_RGB:[-1,-1,-1],                 $  ;显示区域中RGB三通道对应的文件ID
    viewBand:-1,                            $  ;显示区域对应的波段数
    viewBand_RGB:[-1,-1,-1],                $  ;显示区域中RGB对于的波段数
    iconBmp:iconBmp,                        $
    ImagePath:ImagePath,                    $
    curPath:rootdir,                        $  ;存储当前路径，用户可在自己功能中引用和赋值
    viewIdx:0,                              $  ;选择、手、放大、缩小、实际像素、适应屏幕
    oCursorData:OBJ_NEW(),                  $
    ToolGroup:ToolGroup,                    $
    Toolbmp:Toolbmp})
    
  WIDGET_CONTROL, tlb, set_uvalue = pState
  
  XMANAGER, 'idlviewer', tlb, /no_block
  
END