pro Landsat8_Stats_Interface_event,event
  COMPILE_OPT idl2
  common a,pointer
  case tag_names(event,/structure_name) of
    "WIDGET_KILL_REQUEST" : BEGIN
      tmp=dialog_message("确认关闭",/question)
      if tmp eq "Yes" then begin
        widget_control,event.top,/destroy
        return
      endif
    end
    else: print,"others"
  endcase
  widget_control,event.id,get_uvalue=uvalue
  case uvalue of
    'adc' : begin
      inputfiles = DIALOG_PICKFILE(title='select files......',/multiple_files)
      (*pointer).inputfiles=inputfiles
      widget_control,(*pointer).img_txt,set_value=inputfiles
    end

    'save' : begin
      out_name=dialog_pickfile(title='预处理结果保存路径...',/directory)
      (*pointer).out_name=out_name
      widget_control,(*pointer).save_txt,set_value=out_name
    end
    'confirm' : begin
      count=n_elements((*pointer).inputfiles)
      inputpath=file_dirname((*pointer).inputfiles[0])+'\'
      outputpath=(*pointer).out_name
      for i=0,count-1 do begin
        file=(*pointer).inputfiles[i]
        basename=file_basename(file,'.tif')
        output=outputpath+basename+'_'
        outname1=outputpath+basename+'sattic.txt'
        if file eq '' then begin
          return
        endif        
        envi_open_file, file, r_fid=fid
        if (fid eq -1) then begin
          envi_batch_exit
          return
        endif

        envi_file_query, fid, dims=dims, nb=nb
        pos = lindgen(nb)
        envi_doit, 'envi_stats_doit', fid=fid, pos=pos, $
          dims=dims, comp_flag=3, dmin=dmin, dmax=dmax, $
          mean=mean, stdv=stdv, hist=hist
        
        print, 'Bands ', nb       
        print, 'Minimum ', dmin
        print, 'Maximum ', dmax
        print, 'Mean ', mean
        print, 'Standard Deviation ', stdv
        openw,lun,outname1,/get_lun
        printf,lun,'波段数'
        printf,lun,nb
        printf,lun,'均值'
        printf,lun,mean
        printf,lun,'最大值'
        printf,lun,dmax
        printf,lun,'最小值'
        printf,lun,dmin

        printf,lun,'方差'
        printf,lun,stdv
        free_lun,lun
        mes4=dialog_message('图像基本信息统计完成!',/info)
        
      endfor
      widget_control,event.top,/destroy
    end
  endcase
end

pro Landsat8_Stats_Interface,ev
  ;影像拼接程序
  ;界面设计
  COMPILE_OPT idl2
  ENVI,/restore_base_save_files
  ENVI_BATCH_INIT
  adctlb=WIDGET_BASE(title = "图像基本信息统计", $
    xsize=500,ysize=300,/Tlb_Kill_Request_Events,$
    tlb_frame_attr=1)

  imgbase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=20)
  label1=widget_label(imgbase,value='Landsat8数据路径:',xoffset=5,yoffset=10)
  img_dir=widget_button(imgbase,value='选择Landsat8数据',xoffset=160,yoffset=5,uvalue='adc')
  wlabel1=widget_label(imgbase,value='文件列表',xoffset=240,yoffset=35)
  img_txt=widget_list(imgbase,xoffset=5,yoffset=50,xsize=80,ysize=12)

  savebase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=220)
  savedir=widget_button(savebase,xoffset=5,yoffset=5,value='保存信息统计结果为:',uvalue='save')
  save_txt=widget_text(savebase,xsize=300,yoffset=5,xoffset=120)

  confirmbutton=widget_button(adctlb,xoffset=210,yoffset=260,value='确认',xsize=70,ysize=35,uvalue='confirm')
  ;设置参数保存结构体
  common a,pointer
  state = {adctlb:adctlb, $
    ;  tlb : tlb, $
    label1: label1, $
    wlabel1: wlabel1, $
    img_dir: img_dir, $
    img_txt : img_txt, $
    save_txt:save_txt,$

    ; map_info: create_struct(a,2),$
    out_name:'',inputfiles:strarr(10)}
  pointer=ptr_new(/ALLOCATE_HEAP)
  *pointer=state

  widget_control,adctlb,/realize,/no_copy
  XMANAGER, 'Landsat8_Stats_Interface', adctlb, /NO_BLOCK
end