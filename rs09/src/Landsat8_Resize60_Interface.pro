pro Landsat8_Resize60_Interface_event,event
  COMPILE_OPT idl2
  common a,pointer
  case tag_names(event,/structure_name) of
    "WIDGET_KILL_REQUEST" : BEGIN
      tmp=dialog_message("确认关闭",/question)
      if tmp eq "Yes" then begin
        widget_control,event.top,/destroy
        return
      endif
    end
    else: print,"others"
  endcase

  widget_control,event.id,get_uvalue=uvalue
  case uvalue of
    'adc' : begin
      inputfiles = DIALOG_PICKFILE(title='select files......',/multiple_files)

      (*pointer).inputfiles=inputfiles

      widget_control,(*pointer).img_txt,set_value=inputfiles

    end

    'shp' : begin
      shapefile=dialog_pickfile(title='select files......')
      if shapefile eq '' then begin
        mes2=dialog_message('No shape file has been selected!',/error)
        return
      endif

      (*pointer).shapefile=shapefile

      widget_control,(*pointer).dem_txt,set_value=shapefile

    end

    'save' : begin
      outname=dialog_pickfile(title='结果保存路径...',/directory)
      (*pointer).outname=outname
      widget_control,(*pointer).save_txt,set_value=outname
    end

    'confirm' : begin


      count=n_elements((*pointer).inputfiles)

      inputpath=file_dirname((*pointer).inputfiles[0])+'\'

;      outputpath=file_dirname((*pointer).outname)+'\'
      
      outputpath = (*pointer).outname
      
      print,outputpath

      for i=0,count-1 do begin
        inputfile=(*pointer).inputfiles[i]
        filebasename=file_basename(inputfile,'.txt')
        
        outputfile = outputpath + filebasename + '_rad.dat'
        bilFile = outputpath + filebasename + '_rad_BIL.dat'
        outputfile1 = outputpath + filebasename + '_rad.tif'
        
        if (inputfile ne '') then begin
          
          COMPILE_OPT IDL2
          ; Start the application
          e = ENVI(/HEADLESS)
          Raster = e.OpenRaster(inputfile)
          ; Get the radiometric calibration task from the catalog of ENVI tasks.
          Task = ENVITask('RadiometricCalibration')
          ; Define inputs. Since radiance is the default calibration method
          ; you do not need to specify it here.
          Task.Input_Raster = Raster[0] ; Bands 1-7
          Task.Output_Data_Type = 'Float'
          ; Define output raster URI
          Task.Output_Raster_URI = outputfile
          Task.SCALE_FACTOR =0.1
          ; Run the task
          Task.Execute
          ;Task.OUTPUT_RASTER.Export, bilFile, 'envi', INTERLEAVE='bil'

          transftif, outputfile, outputfile1

          e.Close
      
             
        endif
        
      endfor
      
      mes4=dialog_message('预处理完成!',/info)

      widget_control,event.top,/destroy

    end
  endcase
end

pro Landsat8_Resize60_Interface,ev

  ;影像拼接程序
  ;界面设计

  COMPILE_OPT idl2
  ENVI,/restore_base_save_files
  ENVI_BATCH_INIT
  adctlb=WIDGET_BASE(title = "影像预处理流程", $
    xsize=600,ysize=400,/Tlb_Kill_Request_Events,$
    tlb_frame_attr=1)


  imgbase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=20)
  label1=widget_label(imgbase,value='遥感影像路径:',xoffset=5,yoffset=10)
  img_dir=widget_button(imgbase,value='选择遥感影像数据',xoffset=120,yoffset=5,uvalue='adc')

  wlabel1=widget_label(imgbase,value='文件列表',xoffset=240,yoffset=50)

  img_txt=widget_list(imgbase,xoffset=5,yoffset=80,xsize=80,ysize=12)


;  dembase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=236)
;  label2=widget_label(dembase,value='研究区土地利用数据路径',xoffset=200,yoffset=10)
;  demdir=widget_button(dembase,value="选择研究区土地利用数据",xoffset=5,yoffset=40,uvalue='shp')
;  dem_txt=widget_text(dembase,xsize=300,yoffset=40,xoffset=170)


  savebase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=320)
  savedir=widget_button(savebase,xoffset=5,yoffset=5,value='保存预处理结果为:',uvalue='save')
  save_txt=widget_text(savebase,xsize=300,yoffset=6,xoffset=160)

  confirmbutton=widget_button(adctlb,xoffset=280,yoffset=360,value='确认',xsize=70,ysize=35,uvalue='confirm')

  ;设置参数保存结构体
  common a,pointer
  state = {adctlb:adctlb, $
    ;  tlb : tlb, $
    label1: label1, $
;    label2: label2, $
;    demdir: demdir, $
;    dem_txt: dem_txt, $
    wlabel1: wlabel1, $
    img_dir: img_dir, $
    img_txt : img_txt, $
    save_txt:save_txt,$

    ; map_info: create_struct(a,2),$
    outname:'',inputfiles:strarr(10),shapefile:''}
  pointer=ptr_new(/ALLOCATE_HEAP)
  *pointer=state


  widget_control,adctlb,/realize,/no_copy
  XMANAGER, 'Landsat8_Resize60_Interface', adctlb, /NO_BLOCK
end




