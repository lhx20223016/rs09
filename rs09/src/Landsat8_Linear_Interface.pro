pro Landsat8_Linear_Interface_event,event
  COMPILE_OPT idl2
  common a,pointer
  case tag_names(event,/structure_name) of
    "WIDGET_KILL_REQUEST" : BEGIN
      tmp=dialog_message("确认关闭",/question)
      if tmp eq "Yes" then begin
        widget_control,event.top,/destroy
        return
      endif
    end
    else: print,"others"
  endcase

  widget_control,event.id,get_uvalue=uvalue
  case uvalue of
    'adc' : begin
      inputfiles = DIALOG_PICKFILE(title='select files......',/multiple_files)
      (*pointer).inputfiles=inputfiles
      widget_control,(*pointer).img_txt,set_value=inputfiles
    end

    'shp' : begin
      shapefile=dialog_pickfile(title='select files......')
      if shapefile eq '' then begin
        mes2=dialog_message('No shape file has been selected!',/error)
        return
      endif
      (*pointer).shapefile=shapefile
      widget_control,(*pointer).dem_txt,set_value=shapefile
    end

    'save' : begin
      outname=dialog_pickfile(title='结果保存路径...',/directory)
      (*pointer).outname=outname
      widget_control,(*pointer).save_txt,set_value=outname
    end

    'confirm' : begin
      count=n_elements((*pointer).inputfiles)
      inputpath=file_dirname((*pointer).inputfiles[0])+'\'

      outputpath = (*pointer).outname
      print,outputpath
      for i=0,count-1 do begin
        inputfile=(*pointer).inputfiles[i]
        filebasename=file_basename(inputfile,'.tif')
        out_name = outputpath + filebasename + '_Linear.dat'
        out_name1 = outputpath + filebasename + '_Linear.tif'

        envi_open_file,inputfile,r_fid=fid_nor
        if (fid_nor eq -1) then begin
          envi_batch_exit
          return
        endif
        if(fid_nor eq -1)then return
        ENVI_FILE_QUERY,fid_nor,ns=ns_nor,nl=nl_nor,nb=nb_nor,dims=dims_nor
        map_info=envi_get_map_info(fid=fid_nor)
        pos_nor=lindgen(nb_nor)
        
        envi_doit, 'stretch_doit', $
          fid=fid_nor, pos=pos_nor, dims=dims_nor, $
          method=1, out_name=out_name, $
          i_min=2.0, i_max=98.0, range_by=0, $
          out_min=0, out_max=255, out_dt=1, $
          r_fid=r_fid

        ENVI_FILE_MNG, ID = fid_nor, /REMOVE
        transftif, out_name, out_name1
        mes4=dialog_message('线性拉伸处理完成!',/info)
      endfor
      widget_control,event.top,/destroy
    end
  endcase
end

pro Landsat8_Linear_Interface,ev
  ;影像拼接
  ;界面设计
  COMPILE_OPT idl2
  ENVI,/restore_base_save_files
  ENVI_BATCH_INIT
  adctlb=WIDGET_BASE(title = "线性拉伸计算流程", $
    xsize=600,ysize=400,/Tlb_Kill_Request_Events,$
    tlb_frame_attr=1)

  imgbase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=20)
  label1=widget_label(imgbase,value='遥感影像路径:',xoffset=5,yoffset=10)
  img_dir=widget_button(imgbase,value='选择遥感影像数据',xoffset=120,yoffset=5,uvalue='adc')
  wlabel1=widget_label(imgbase,value='文件列表',xoffset=240,yoffset=50)
  img_txt=widget_list(imgbase,xoffset=5,yoffset=80,xsize=80,ysize=12)

  savebase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=320)
  savedir=widget_button(savebase,xoffset=5,yoffset=5,value='保存线性拉伸结果为:',uvalue='save')
  save_txt=widget_text(savebase,xsize=300,yoffset=6,xoffset=160)

  confirmbutton=widget_button(adctlb,xoffset=280,yoffset=360,value='确认',xsize=70,ysize=35,uvalue='confirm')

  ;设置参数保存结构体
  common a,pointer
  state = {adctlb:adctlb, $
    ;  tlb : tlb, $
    label1: label1, $
    ;    label2: label2, $
    ;    demdir: demdir, $
    ;    dem_txt: dem_txt, $
    wlabel1: wlabel1, $
    img_dir: img_dir, $
    img_txt : img_txt, $
    save_txt:save_txt,$

    ; map_info: create_struct(a,2),$
    outname:'',inputfiles:strarr(10),shapefile:''}
  pointer=ptr_new(/ALLOCATE_HEAP)
  *pointer=state

  widget_control,adctlb,/realize,/no_copy
  XMANAGER, 'Landsat8_Linear_Interface', adctlb, /NO_BLOCK
end