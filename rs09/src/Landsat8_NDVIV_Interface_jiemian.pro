pro Landsat8_NDVI_Interface_event, event
  COMPILE_OPT idl2
  common a, pointer
  case tag_names(event, /structure_name) of
    "WIDGET_KILL_REQUEST": begin
      tmp = dialog_message("是否要关闭窗口？", /question)
      if tmp eq "Yes" then begin
        widget_control, event.top, /destroy
        return
      endif
    end
    else: print, "others"
  endcase

  widget_control, event.id, get_uvalue=uvalue
  case uvalue of
    'adc': begin
      inputfiles = DIALOG_PICKFILE(title='选择文件......', /multiple_files)

      (*pointer).inputfiles = inputfiles

      widget_control, (*pointer).img_txt, set_value=inputfiles

    end

    'shp': begin
      shapefile = dialog_pickfile(title='选择文件......')
      if shapefile eq '' then begin
        mes2 = dialog_message('没有选择任何shape文件！', /error)
        return
      endif

      (*pointer).shapefile = shapefile

      widget_control, (*pointer).dem_txt, set_value=shapefile

    end

    'save': begin
      outname = dialog_pickfile(title='保存文件到...', /directory)
      (*pointer).outname = outname
      widget_control, (*pointer).save_txt, set_value=outname
    end

    'confirm': begin

      count = n_elements((*pointer).inputfiles)

      inputpath = file_dirname((*pointer).inputfiles[0]) + '\'

      outputpath = file_dirname((*pointer).outname) + '\'

      outputpath = (*pointer).outname

      print, outputpath

      for i=0, count-1 do begin
        inputfile = (*pointer).inputfiles[i]

        filebasename = file_basename(inputfile, '.tif')

        out_name = outputpath + filebasename + '_植被指数.dat'
        out_name1 = outputpath + filebasename + '_植被指数.tif'

        envi_open_file, inputfile, r_fid=fid_nor

        if (fid_nor eq -1) then begin

          envi_batch_exit

          return

        endif

        if(fid_nor eq -1) then return
        ENVI_FILE_QUERY, fid_nor, ns=ns_nor, nl=nl_nor, nb=nb_nor, dims=dims_nor
        map_info = envi_get_map_info(fid=fid_nor)
        pos_nor = lindgen(nb_nor)

        redBand = envi_get_data(fid=fid_nor, dims=dims_nor, pos=3)
        NirBand = envi_get_data(fid=fid_nor, dims=dims_nor, pos=4)

        NDVI = (NirBand/10000.0 - redBand/10000.0) / (NirBand/10000.0 + redBand/10000.0)

        waa = where(NDVI le 0.4)
        NDVI[waa] = 0
        wab = where(NDVI gt 0.4)
        NDVI[wab] = 1

        ENVI_WRITE_ENVI_FILE, NDVI, out_name=out_name, map_info=map_info

        ENVI_FILE_MNG, ID = fid_nor, /REMOVE

        transftif, out_name, out_name1

        mes4 = dialog_message('植被指数计算完成！', /info)

      endfor

      widget_control, event.top, /destroy

    end
  endcase
end

pro Landsat8_NDVI_Interface, ev
  ;创建用户界面
  ;初始化界面

  COMPILE_OPT idl2
  ENVI, /restore_base_save_files
  ENVI_BATCH_INIT
  adctlb = WIDGET_BASE(title = "植被指数计算界面", $
    xsize=600,ysize=400,/Tlb_Kill_Request_Events,$
    tlb_frame_attr=1)

  imgbase = WIDGET_BASE(adctlb, frame=3, xoffset=0, yoffset=20)
  label1 = widget_label(imgbase, value='选择影像文件路径:', xoffset=5, yoffset=10)
  img_dir = widget_button(imgbase, value='选择影像文件路径', xoffset=120, yoffset=5, uvalue='adc')

  wlabel1 = widget_label(imgbase, value='文件列表：', xoffset=240, yoffset=50)

  img_txt = widget_list(imgbase, xoffset=5, yoffset=80, xsize=80, ysize=12)

  ; dembase = WIDGET_BASE(adctlb, frame=3, xoffset=0, yoffset=236)
  ; label2 = widget_label(dembase, value='选择DEM文件路径：', xoffset=200, yoffset=10)
  ; demdir = widget_button(dembase, value="选择DEM文件路径", xoffset=5, yoffset=40, uvalue='shp')
  ; dem_txt = widget_text(dembase, xsize=300, yoffset=40, xoffset=170)

  savebase = WIDGET_BASE(adctlb, frame=3, xoffset=0, yoffset=320)
  savedir = widget_button(savebase, xoffset=5, yoffset=5, value='保存植被指数为：', uvalue='save')
  save_txt = widget_text(savebase, xsize=300, yoffset=6, xoffset=160)

  confirmbutton = widget_button(adctlb, xoffset=280, yoffset=360, value='计算', xsize=70, ysize=35, uvalue='confirm')

  ;初始化指针
  common a, pointer
  state = {adctlb:adctlb, $
    ; tlb : tlb, $
    label1: label1, $
    ; label2: label2, $
    ; demdir: demdir, $
    ; dem_txt: dem_txt, $
    wlabel1: wlabel1, $
    img_dir: img_dir, $
    img_txt : img_txt, $
    save_txt:save_txt,$

    ; map_info: create_struct(a,2),$
    outname:'',inputfiles:strarr(10),shapefile:''}
  pointer = ptr_new(/ALLOCATE_HEAP)
  *pointer = state

  widget_control, adctlb, /realize, /no_copy
  XMANAGER, 'Landsat8_NDVI_Interface', adctlb, /NO_BLOCK
end