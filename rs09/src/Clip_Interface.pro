pro Clip_Interface_event,event
  COMPILE_OPT idl2
  common a,pointer
  case tag_names(event,/structure_name) of
    "WIDGET_KILL_REQUEST" : BEGIN
    tmp=dialog_message("确认关闭",/question)
    if tmp eq "Yes" then begin
      widget_control,event.top,/destroy
      return
    endif
    end
    else: print,"others"
  endcase
  
  widget_control,event.id,get_uvalue=uvalue
  case uvalue of
    'adc' : begin
      inputfiles = DIALOG_PICKFILE(title='select files......',/multiple_files)
      
      (*pointer).inputfiles=inputfiles
      
      widget_control,(*pointer).img_txt,set_value=inputfiles
      
     end
     
     'shp' : begin
      shapefile=dialog_pickfile(filter='*.shp')
      if shapefile eq '' then begin
        mes2=dialog_message('No shape file has been selected!',/error)
        return
      endif
      
     (*pointer).shapefile=shapefile
     
      widget_control,(*pointer).shp_txt,set_value=shapefile
      
     end     
     
     'save' : begin
        ouname=dialog_pickfile(title='裁剪结果保存路径...',/directory)
        (*pointer).ouname=ouname
        widget_control,(*pointer).save_txt,set_value=ouname
      end
     
     'confirm' : begin
       count=n_elements((*pointer).inputfiles)       
       for k=0,count-1 do begin
         theseFile=(*pointer).inputfiles[k]
         basename=file_basename(theseFile,'.tif')
         dirName = FILE_DIRNAME(theseFile)
         ENVI_OPEN_FILE,theseFile,r_fid = fid
         ENVI_FILE_QUERY, fid, ns = ns, nb = nb, nl = nl, dims = dims,BNAMES = BNAMES

         shapeobj = OBJ_NEW('IDLffShape', (*pointer).shapefile)
         shapeobj->GETPROPERTY, N_Entities = nEntities
      
       
         roi_ids = LONARR(nEntities>1)
         FOR i=0, nEntities-1 DO BEGIN
           entitie = shapeobj->GETENTITY(i)
        
           IF (entitie.SHAPE_TYPE EQ 5)  THEN BEGIN
             record = *(entitie.VERTICES)
            
             ENVI_CONVERT_FILE_COORDINATES,fid,xmap,ymap,record[0,*],record[1,*]
        
             roi_ids[i] = ENVI_CREATE_ROI(color=4,  $
               ns = ns ,  nl = nl)
             ENVI_DEFINE_ROI, roi_ids[i], /polygon, xpts=REFORM(xMap), ypts=REFORM(yMap)
           
             IF i EQ 0 THEN BEGIN
               xmin = ROUND(MIN(xMap,max = xMax))
               yMin = ROUND(MIN(yMap,max = yMax))
             ENDIF ELSE BEGIN
               xmin = xMin < ROUND(MIN(xMap))
               xMax = xMax > ROUND(MAX(xMap))
               yMin = yMin < ROUND(MIN(yMap))
               yMax = yMax > ROUND(MAX(yMap))
             ENDELSE
            ENDIF
           shapeobj->DESTROYENTITY, entitie
         ENDFOR
         OBJ_DESTROY, shapeobj
         ;
         xMin = xMin >0
         xmax = xMax < ns-1
         yMin = yMin >0
         ymax = yMax < nl-1

         out_dims = [-1,xMin,xMax,yMin,yMax]
 
         cfg = envi_get_configuration_values()
         tmppath = cfg.DEFAULT_TMP_DIRECTORY

  
         ENVI_MASK_DOIT,$
           AND_OR =1, $
           OUT_NAME = tmppath+path_sep()+'void.mask', $
           ROI_IDS= roi_ids, $ 
           ns = ns, nl = nl, $
           /inside, $ 
           r_fid = m_fid

         ENVI_MASK_APPLY_DOIT, FID = fid, POS = INDGEN(nb), DIMS = out_dims, $
           M_FID = m_fid, M_POS = [0], VALUE = 0, $
           out_name = (*pointer).ouname+basename+'_clip.dat'

         ENVI_FILE_MNG, id =m_fid,/remove
         ENVI_FILE_MNG, id =fid,/remove
         
         transftif, (*pointer).ouname+basename+'_clip.dat', (*pointer).ouname+basename+'_clip.tif'
         
         mes4=dialog_message('裁剪完成!',/info)
         
         break

       endfor
       ;mes4=dialog_message('裁剪完成!',/info)
       widget_control,event.top,/destroy
              
     end
  endcase
end

pro Clip_Interface,ev
  ;影像裁剪程序
  ;界面设计
  COMPILE_OPT idl2
  ENVI,/restore_base_save_files
  ENVI_BATCH_INIT
  adctlb=WIDGET_BASE(title = "Landsat8数据裁剪", $
    xsize=800,ysize=300,/Tlb_Kill_Request_Events,$
    tlb_frame_attr=1)
    
  
  imgbase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=20)
  label1=widget_label(imgbase,value='影像路径:',xoffset=5,yoffset=10)
  img_dir=widget_button(imgbase,value='选择影像数据',xoffset=5,yoffset=40,uvalue='adc')
  img_txt=widget_text(imgbase,value="您还未选择任何文件",xsize=300,yoffset=41,xoffset=120)
    
  shpbase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=110)
  label2=widget_label(shpbase,value='矢量文件路径:',xoffset=5,yoffset=10)
  shpdir=widget_button(shpbase,value="选择矢量数据",xoffset=5,yoffset=40,uvalue='shp')
  shp_txt=widget_text(shpbase,value="您还未选择任何文件",xsize=300,yoffset=41,xoffset=120)
  
  
  savebase=WIDGET_BASE(adctlb,frame=3,xoffset=0,yoffset=200)
  savedir=widget_button(savebase,xoffset=5,yoffset=5,value='保存裁剪结果为:',uvalue='save')
  save_txt=widget_text(savebase,xsize=300,yoffset=6,xoffset=145)
  
  confirmbutton=widget_button(adctlb,xoffset=350,yoffset=242,value='确认',xsize=100,ysize=50,uvalue='confirm')
  
  ;设置参数保存结构体
  common a,pointer
  state = {adctlb:adctlb, $
    ;  tlb : tlb, $
    label1: label1, $
    img_dir: img_dir, $
    img_txt : img_txt, $
    shpdir : shpdir , $
    shp_txt : shp_txt,$
    save_txt:save_txt,$

   ; map_info: create_struct(a,2),$
    ouname:'',inputfiles:strarr(10),shapefile:''}
  pointer=ptr_new(/ALLOCATE_HEAP)
  *pointer=state

  
  widget_control,adctlb,/realize,/no_copy
  XMANAGER, 'Clip_Interface', adctlb, /NO_BLOCK
end


